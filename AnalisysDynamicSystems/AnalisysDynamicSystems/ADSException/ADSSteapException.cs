﻿namespace AnalisysDynamicSystems.ADSException
{
    public class ADSSteapException : AnalisysDynamicSystemsException
    {
        public ADSSteapException(float value): base(string.Format(MESSAGE_STEAP_ERROR, value)) { }
    }
}
