﻿namespace AnalisysDynamicSystems.ADSException
{
    public class AnalisysDynamicSystemsException : Exception
    {
        protected const string
            MESSAGE_STEAP_ERROR = "Значение {0} поля 'Steap' было слишком велико или слишком мало",
            MESSAGE_VECTOR_ERROR = "Vector3 {0} является не коректным для дальнейшего использования",
            MESSAGE_ERROR_DEFAULT = "Возникла ошибка в работе AnalisysDynamicSystems";

        public AnalisysDynamicSystemsException(string? message)
            : base(message ?? MESSAGE_ERROR_DEFAULT) { }
    }
}
