﻿using System.Numerics;

namespace AnalisysDynamicSystems.ADSException
{
    public class ADSVectorException : AnalisysDynamicSystemsException
    {
        public ADSVectorException(Vector3 vector) : base(string.Format(MESSAGE_VECTOR_ERROR, vector)) { }
    }
}
