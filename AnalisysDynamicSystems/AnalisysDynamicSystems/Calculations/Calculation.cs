﻿using AnalisysDynamicSystems.DynamicSystems;

namespace AnalisysDynamicSystems.Calculations
{
    public abstract class Calculation<T>
    {
        public float Steap { get; set; } = 0.001f;
        public uint CountIteration { get; set; } = 1_000_000;
        public DynamicSystem CurrentDynamicSystem { get; private set; }

        public Calculation(DynamicSystem CurrentDynamicSystem)
        {
            this.CurrentDynamicSystem = CurrentDynamicSystem;
        }
        public abstract T Act();
    }
}
