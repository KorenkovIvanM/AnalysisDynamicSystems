﻿using AnalisysDynamicSystems.DynamicSystems;
using System.Numerics;

namespace AnalisysDynamicSystems.Calculations
{
    public class DefaultCalculation : Calculation<Vector3>
    {
        public DefaultCalculation(DynamicSystem CurrentDynamicSystem) 
            : base(CurrentDynamicSystem) { }

        public Vector3 Vector { get; set; }
        public float Steap { get; set; } = 0;

        public override Vector3 Act() => CurrentDynamicSystem.GetNextVector(Vector, Steap);
    }
}
