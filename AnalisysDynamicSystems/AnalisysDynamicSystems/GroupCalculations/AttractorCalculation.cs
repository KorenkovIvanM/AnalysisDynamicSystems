﻿using AnalisysDynamicSystems.Calculations;
using System.Numerics;

namespace AnalisysDynamicSystems.GroupCalculations
{
    public class AttractorCalculation : GroupCalculation<byte>
    {
        public AttractorCalculation(Calculation<byte> calculation) : base(calculation)
        {
        }

        protected float MaxX { get; set; } = float.MinValue;
        protected float MaxY { get; set; } = float.MinValue;
        protected float MaxZ { get; set; } = float.MinValue;
        protected float MinX { get; set; } = float.MaxValue;
        protected float MinY { get; set; } = float.MaxValue;
        protected float MinZ { get; set; } = float.MaxValue;
        protected uint CountIterationSize { get; set; } = 10_000;
        protected float SteapSize { get; set; } = 0.1f;

        public uint CountIteration { get; set; } = 1_000_000;
        public float Steap { get; set; } = 0.001f;
        public new DefaultCalculation CurrentCalculation { get; private set; }

        public void SetSize()
        {
            var vector = CurrentCalculation.CurrentDynamicSystem.GetDefaultStartState();
            CurrentCalculation.Steap = SteapSize;


            for (int i = 1; i < CountIteration; i++)
            {
                CurrentCalculation.Vector = vector;
                vector = CurrentCalculation.Act();

                if (vector.X < MinX) MinX= vector.X;
                if (vector.Y < MinY) MinY= vector.Y;
                if (vector.Z < MinZ) MinZ= vector.Z;
                if (vector.X > MaxX) MaxX= vector.X;
                if (vector.Y > MaxY) MaxY= vector.Y;
                if (vector.Z > MaxZ) MaxZ= vector.Z;
            }
        }

        public override byte[,] GetMatrix()
        {
            Vector3[] line = new Vector3[CountIteration];
            line[0] = CurrentCalculation.CurrentDynamicSystem.GetDefaultStartState();
            CurrentCalculation.Steap = Steap;


            for(int i = 1; i < CountIteration; i++)
            {
                line[i] = CurrentCalculation.Act();
                CurrentCalculation.Vector = line[i];
            }

            return new byte[1,1];
        }
    }
}
