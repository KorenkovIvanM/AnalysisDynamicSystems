﻿using AnalisysDynamicSystems.Calculations;
using AnalisysDynamicSystems.Result;

namespace AnalisysDynamicSystems.GroupCalculations
{
    public abstract class GroupCalculation<T> : IResult<T>
    {
        protected Calculation<T> CurrentCalculation { get; private set; }
        public abstract T[,] GetMatrix();

        public GroupCalculation(Calculation<T> calculation)
        {
            CurrentCalculation = calculation;
        }
    }
}
