﻿namespace AnalisysDynamicSystems.Result
{
    public interface IResult<T>
    {
        T[,] GetMatrix();
    }
}
