﻿using AnalisysDynamicSystems.ADSException;
using System.Numerics;

namespace AnalisysDynamicSystems.DynamicSystems
{
    public abstract class DynamicSystem
    {
        public readonly string Name;
        protected Dictionary<string, float> Parametrs;

        public DynamicSystem(string name)
        {
            Name = name;
            Parametrs = GetDefaultParametrs();
        }

        public abstract float Fx(Vector3 v);
        public abstract float Fy(Vector3 v);
        public abstract float Fz(Vector3 v);

        public Vector3 GetNextVector(Vector3 point, float steap)
        {
            if (
                float.IsNaN(steap) ||
                float.IsInfinity(steap) ||
                steap >= 10 ||
                steap < 0.00000001f) 
                throw new ADSSteapException(steap);

            float[,] k = new float[4, 3];

            k[0, 0] = Fx(point);
            k[0, 1] = Fy(point);
            k[0, 2] = Fz(point);

            k[1, 0] = Fx(new Vector3(point.X + steap / 2, point.Y + steap / 2 * k[0, 1], point.Z + steap / 2 * k[0, 2]));
            k[1, 1] = Fy(new Vector3(point.X + steap / 2 * k[0, 0], point.Y + steap / 2, point.Z + steap / 2 * k[0, 2]));
            k[1, 2] = Fz(new Vector3(point.X + steap / 2 * k[0, 0], point.Y + steap / 2 * k[0, 1], point.Z + steap / 2));

            k[2, 0] = Fx(new Vector3(point.X + steap / 2, point.Y + steap / 2 * k[1, 1], point.Z + steap / 2 * k[1, 2]));
            k[2, 1] = Fy(new Vector3(point.X + steap / 2 * k[1, 0], point.Y + steap / 2, point.Z + steap / 2 * k[1, 2]));
            k[2, 2] = Fz(new Vector3(point.X + steap / 2 * k[1, 0], point.Y + steap / 2 * k[1, 1], point.Z + steap / 2));

            k[3, 0] = Fx(new Vector3(point.X + steap, point.Y + steap * k[2, 1], point.Z + steap * k[2, 2]));
            k[3, 1] = Fy(new Vector3(point.X + steap * k[2, 0], point.Y + steap, point.Z + steap * k[2, 2]));
            k[3, 2] = Fz(new Vector3(point.X + steap * k[2, 0], point.Y + steap * k[2, 1], point.Z + steap));

            point.X += steap / 6 * (k[0, 0] + 2 * k[1, 0] + 2 * k[2, 0] + k[3, 0]);
            point.Y += steap / 6 * (k[0, 1] + 2 * k[1, 1] + 2 * k[2, 1] + k[3, 1]);
            point.Z += steap / 6 * (k[0, 2] + 2 * k[1, 2] + 2 * k[2, 2] + k[3, 2]);

            return point;
        }
        public virtual Vector3 GetDefaultStartState() 
            => new Vector3(0.01f, 0, 0);
        public abstract Dictionary<string, float> GetDefaultParametrs();
        public virtual List<string> GetListParametrs() 
            => GetDefaultParametrs()
                .Select(item => item.Key)
                .ToList();
    }
}
