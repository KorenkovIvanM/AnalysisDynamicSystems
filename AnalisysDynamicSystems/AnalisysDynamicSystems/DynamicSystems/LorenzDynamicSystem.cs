﻿using System.Numerics;

namespace AnalisysDynamicSystems.DynamicSystems
{
    public class LorenzDynamicSystem : DynamicSystem
    {
        public const string
            SIGMA = "Sigma", R = "R", B = "B",
            NAME_SYSTEMS = "Lorenz systems";

        public LorenzDynamicSystem() : base(NAME_SYSTEMS) { }

        public override float Fx(Vector3 v)
        {
            throw new NotImplementedException();
        }

        public override float Fy(Vector3 v)
        {
            throw new NotImplementedException();
        }

        public override float Fz(Vector3 v)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, float> GetDefaultParametrs()
        {
            throw new NotImplementedException();
        }
    }
}
