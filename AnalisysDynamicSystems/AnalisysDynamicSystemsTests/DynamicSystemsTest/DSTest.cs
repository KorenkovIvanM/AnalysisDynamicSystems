﻿using AnalisysDynamicSystems.DynamicSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisysDynamicSystemsTests.DynamicSystemsTest
{
    internal class DSTest : DynamicSystem
    {
        public override float Fx(Vector3 v) => v.X;
        public override float Fy(Vector3 v) => v.Y;
        public override float Fz(Vector3 v) => v.Z;

        public override Dictionary<string, float> GetDefaultParametrs() 
            => new Dictionary<string, float>();

        public DSTest() : base(typeof(DSTest).ToString()) { }
    }
}
