﻿using AnalisysDynamicSystems.ADSException;
using AnalisysDynamicSystems.DynamicSystems;
using AnalisysDynamicSystemsTests.DynamicSystemsTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Numerics;

namespace AnalisysDynamicSystems.Tests
{
    

    [TestClass]
    public class DynamicSystem_Tests
    {
        private const string
        NAME_TEST_DS = "Test DS";

        [TestMethod]
        public void GetNextVector_return_1_Test()
        {
            // Arrange
            Mock<DynamicSystem> DSTest = new(NAME_TEST_DS);
            DSTest.Setup(item => item.Fx(It.IsAny<Vector3>())).Returns(1f);
            DSTest.Setup(item => item.Fy(It.IsAny<Vector3>())).Returns(1f);
            DSTest.Setup(item => item.Fz(It.IsAny<Vector3>())).Returns(1f);

            // Act
            var result = DSTest.Object.GetNextVector(new Vector3(), 0.001f);
            Console.WriteLine(result);

            // Assert
            DSTest.VerifyAll();
            Assert.AreEqual(new Vector3(0.001f, 0.001f, 0.001f), result);
        }

        [TestMethod]
        public void GetNextVector_DSTest_Time_1_return_e()
        {
            // Arrange
            DSTest ds = new();
            Vector3 vector = new Vector3(1, 1, 1);
            int countIteration = 1_000_000;

            // Act
            for (int i = 0; i < countIteration; i++)
                vector = ds.GetNextVector(vector, 1f / countIteration);


            // Assert
            Assert.IsTrue(Math.Abs(vector.X - Math.E) < 0.01);
        }

        [TestMethod]
        [ExpectedException(typeof(ADSSteapException))]
        public void GetNextVector_Steap_non_correct_steap_eq_zero()
        {
            // Arrange
            DSTest ds = new();
            Vector3 vector = new Vector3(1, 1, 1);
            float steap = 0f;

            // Act
            ds.GetNextVector(vector, steap);
        }

        [TestMethod]
        [ExpectedException(typeof(ADSSteapException))]
        public void GetNextVector_Steap_non_correct_steap_mi_0_000_000_01()
        {
            // Arrange
            DSTest ds = new();
            Vector3 vector = new Vector3(1, 1, 1);
            float steap = 0.000000001f;

            // Act
            ds.GetNextVector(vector, steap);
        }

        [TestMethod]
        [ExpectedException(typeof(ADSSteapException))]
        public void GetNextVector_Steap_non_correct_steap_eq_nan()
        {
            // Arrange
            DSTest ds = new();
            Vector3 vector = new Vector3(1, 1, 1);
            float steap = float.NaN;

            // Act
            ds.GetNextVector(vector, steap);
        }

        [TestMethod]
        [ExpectedException(typeof(ADSSteapException))]
        public void GetNextVector_Steap_non_correct_steap_ma_10()
        {
            // Arrange
            DSTest ds = new();
            Vector3 vector = new Vector3(1, 1, 1);
            float steap = 12;

            // Act
            ds.GetNextVector(vector, steap);
        }

        [TestMethod]
        [ExpectedException(typeof(ADSSteapException))]
        public void GetNextVector_Steap_non_correct_steap_eq_infinity()
        {
            // Arrange
            DSTest ds = new();
            Vector3 vector = new Vector3(1, 1, 1);
            float steap = float.PositiveInfinity;

            // Act
            ds.GetNextVector(vector, steap);
        }
    }
}