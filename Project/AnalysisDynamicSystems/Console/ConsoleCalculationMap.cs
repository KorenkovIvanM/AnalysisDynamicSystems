﻿using AnalysisDynamicSystems.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThisConsole
{
    internal static class ConsoleCalculationMap<T>
    {
        private static string 
            MESSAGE_START = "Начало выполнения задачи";
        private static int
            s_consoleCursorWidth,
            s_consoleCursorHeight,
            s_countIteration;
        private static DateTime
            s_start;
        private static object
            s_lock = new();

        static ConsoleCalculationMap()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            s_start = DateTime.Now;
            DrawLine();
            Console.WriteLine(new string(' ', (Console.WindowWidth - MESSAGE_START.Length) / 2) +  MESSAGE_START);
            DrawLine();
            Console.WriteLine();
            s_consoleCursorWidth = Console.CursorLeft;
            s_consoleCursorHeight = Console.CursorTop;
        }

        internal static void Linker_onSave(uint index, ref Map map, ref T[] result)
        {
            lock (s_lock)
            {
                s_countIteration++;
                var procent = Convert.ToInt32(Math.Round((Console.WindowWidth - 3) * (Convert.ToDouble(s_countIteration) / map.Width)));
                Console.SetCursorPosition(s_consoleCursorWidth, s_consoleCursorHeight);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(string.Format("[{0}>{1}]", new string('=', procent), new string(' ', Console.WindowWidth - procent - 3)));
                Console.ForegroundColor = ConsoleColor.Green;
                var currentTimeSeconds = (DateTime.Now - s_start).TotalSeconds;

                Console.WriteLine();
                DrawLine();

                Console.WriteLine("Прошло:   " + DoubleToTime(currentTimeSeconds));
                Console.WriteLine("Всего:    " + DoubleToTime(currentTimeSeconds / s_countIteration * map.Width));
                Console.WriteLine("Осталось: " + DoubleToTime(currentTimeSeconds / s_countIteration * map.Width - currentTimeSeconds));
            }
        }

        private static string DoubleToTime(double value)
        {
            return string.Format("{0} часов {1} минут {2} секунд    ", 
                Math.Round(value/3600),
                Math.Round(value / 60),
                Math.Round(value % 60));
        }

        private static void DrawLine()
        {
            Console.WriteLine(new string('-', Console.WindowWidth));
        }
    }
}
