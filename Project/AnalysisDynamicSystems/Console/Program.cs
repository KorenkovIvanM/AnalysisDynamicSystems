﻿using AnalysisDynamicSystems;
using AnalysisDynamicSystems.Calculations.Examples.Attractor;
using AnalysisDynamicSystems.Calculations.Examples.AttractorLyapynovExponentColor;
using AnalysisDynamicSystems.Calculations.Examples.Niding;
using AnalysisDynamicSystems.Core;
using AnalysisDynamicSystems.DynamicSystems.Examples;
using AnalysisDynamicSystems.Linkers.Examples;
using AnalysisDynamicSystems.Models;
using AnalysisDynamicSystems.Visualisations.Examples;
using AnalysisDynamicSystems.Visualisations.Examples.NidingVisualisation;
using ThisConsole;


uint depth = 16;
ShimizyMoriokaDynamicSystem ds = new();
ds[ShimizyMoriokaDynamicSystem.LAMBDA] = 0.8f;
ds[ShimizyMoriokaDynamicSystem.ALPHA] = 0.35f;
AttractorLyapynovExponentColorCalculation calculation = new(ds);
//DefaultAttractorCalculation calculation = new(ds);
calculation.CountIteration = 1_000_000;
calculation.Steap = 0.01f;
Map map = new()
{
    Width = 100,
    Height = 100,
    Name = "Name",
    DynamicSystem = nameof(ds),
    Calculation = nameof(calculation),
    WidthParametrName = ShimizyMoriokaDynamicSystem.LAMBDA,
    HeightParametrName = ShimizyMoriokaDynamicSystem.ALPHA,
    WidthParametrBegin = 0,
    HeightParametrBegin = 0,
    WidthParametrEnd = 1,
    HeightParametrEnd = 0.7f,
};
SaverBD.WriteMap(map);
var linker = new AttractorLyapynovExponentColorLinker(400, 400, calculation);
//var linker = new AttractorLinker(400, 400, calculation);
//linker.onSave += ConsoleCalculationMap<uint>.Linker_onSave;
//linker.onSave += SaverBD.Saver<uint>;



ColorAttractorVisualisation visualisation = new(linker);
//DefaultAttractorVisualisation visualisation = new(linker);

var creater = new CreaterImg();

// Act
var file = new FileInfo(creater.Create(visualisation.GetColorImg()));