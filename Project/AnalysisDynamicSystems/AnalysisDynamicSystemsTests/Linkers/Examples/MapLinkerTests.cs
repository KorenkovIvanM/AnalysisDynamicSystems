﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalysisDynamicSystemsTests;
using AnalysisDynamicSystems.Calculations.Examples.Niding;
using AnalysisDynamicSystems.Models;

namespace AnalysisDynamicSystems.Linkers.Examples.Tests
{
    [TestClass]
    public class MapLinkerTests
    {
        [TestMethod]
        public void GetMatrixTest()
        {
            // Arrange 
            uint depth = 10;
            DSTest ds = new DSTest();
            NidingCalculation calculation = new(ds, depth);
            Map map = new()
            {
                Width = 100,
                Height = 100,
                Name = "Name",
                DynamicSystem = nameof(ds),
                Calculation = nameof(calculation),
                WidthParametrName = DSTest.A,
                HeightParametrName = DSTest.B,
                WidthParametrBegin = 0,
                HeightParametrBegin = 0,
                WidthParametrEnd = 0,
                HeightParametrEnd = 0,
            };
            MapLinker linker = new MapLinker(map, calculation);

            // Act
            var result = linker.GetMatrix();

            // Assert
            for(int i = 0; i < result.GetLength(0); i++)
                for(int j = 0; j < result.GetLength(1); j++)
                    Assert.IsTrue(result[i, j] == Math.Pow(2, depth) - 1);
        }
    }
}