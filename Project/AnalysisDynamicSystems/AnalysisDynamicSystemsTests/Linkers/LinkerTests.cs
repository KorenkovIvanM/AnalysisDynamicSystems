﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalysisDynamicSystems.Linkers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalysisDynamicSystems.Linkers.Examples;
using AnalysisDynamicSystems.Calculations.Examples.Attractor;
using AnalysisDynamicSystemsTests;

namespace AnalysisDynamicSystems.Linkers.Tests
{
    [TestClass]
    public class LinkerTests
    {
        [TestMethod]
        public void GetMatrix_set_box_1_e()
        {
            // Arrange
            var system = new DSTest();
            var calculation = new DefaultAttractorCalculation(system);
            calculation.CountIteration = 1000;
            calculation.Steap = 0.001f;
            var linker = new AttractorLinker(100, 100, calculation);


            // Act
            linker.GetMatrix();

            // Assert
            Assert.IsTrue(Math.Abs(calculation.MinX - 1) < 0.01);
            Assert.IsTrue(Math.Abs(calculation.MaxX - Math.E) < 0.01);
        }

        [TestMethod]
        public void GetMatrixTest()
        {
            // Arrange
            var system = new DSTest();
            var calculation = new DefaultAttractorCalculation(system);
            calculation.CountIteration = 1000;
            calculation.Steap = 0.001f;
            var linker = new AttractorLinker(100, 100, calculation);


            // Act
            var result = linker.GetMatrix();
            Console.WriteLine(result[0, 0]);

            // Assert
            Assert.IsTrue(result[0,0] == 1);
        }
    }
}