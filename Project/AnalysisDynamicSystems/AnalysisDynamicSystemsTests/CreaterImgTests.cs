﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalysisDynamicSystems.DynamicSystems.Examples;
using AnalysisDynamicSystems.Calculations.Examples.Attractor;
using AnalysisDynamicSystems.Linkers.Examples;
using AnalysisDynamicSystems.Visualisations.Examples;

namespace AnalysisDynamicSystems.Tests
{
    [TestClass]
    public class CreaterImgTests
    {
        [TestMethod]
        public void CreateTest_Lorenz_DS()
        {
            // Arrange
            var system = new LorenzDynamicSystems();
            var calculation = new DefaultAttractorCalculation(system);
            calculation.CountIteration = 10_000;
            calculation.Steap = 0.001f;
            var linker = new AttractorLinker(400, 400, calculation);
            var visualisation = new DefaultAttractorVisualisation(linker);
            var creater = new CreaterImg();

            // Act
            var file = new FileInfo(creater.Create(visualisation.GetColorImg()));

            Console.WriteLine(file.FullName);
            // Assert
            Assert.IsTrue(file.Exists);
        }
    }
}