﻿using AnalysisDynamicSystems.Calculations.Examples.Niding;
using AnalysisDynamicSystems.DynamicSystems;
using System.Numerics;

namespace AnalysisDynamicSystemsTests
{
    internal class DSTest : DynamicSystem, INidingDynamicSystem
    {
        public const string
            A = "A", B = "B", C = "C";

        public override float Fx(Vector3 v) => this[A] * v.X;
        public override float Fy(Vector3 v) => this[B] * v.Y;
        public override float Fz(Vector3 v) => this[C] * v.Z;

        public override Dictionary<string, float> GetDefaultParametrs()
            => new Dictionary<string, float>()
            {
                        { A, 1f },
                        { B, 1f },
                        { C, 1f },
            };

        public override Vector3 GetDefaultStartVector() => new Vector3(1, 1, 1);

        public bool IsCritc(Vector3 vBegin, Vector3 bEnd) => true;

        public byte GetCurrentVector(Vector3 currentVector) => (byte)1;

        public DSTest() : base() { }
    }

}
