﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalysisDynamicSystems.DynamicSystems;
using AnalysisDynamicSystemsTests;

namespace AnalysisDynamicSystems.Calculations.Examples.Attractor.Tests
{
    [TestClass]
    public class AttractorCalculationTests
    {
        [TestMethod]
        public void SetBoxExist_Test_Dynamic_System_1_e()
        {
            // Arrange
            DynamicSystem dynamicSystem = new DSTest();
            var calculation = new DefaultAttractorCalculation(dynamicSystem);

            calculation.SteapBox = 0.001f;
            calculation.CountIterationBox = 1000;

            // Act
            calculation.SetBoxExistLongTime();

            // Assert
            Assert.IsTrue(Math.Abs(calculation.MaxX - Math.E) < 0.01);
            Assert.IsTrue(Math.Abs(calculation.MinX - 1) < 0.01);

        }
    }
}