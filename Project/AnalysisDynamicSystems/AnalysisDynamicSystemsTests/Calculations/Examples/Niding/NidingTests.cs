﻿using AnalysisDynamicSystems.Calculations.Examples.Niding;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AnalysisDynamicSystemsTests;
using AnalysisDynamicSystems.DynamicSystems;
using AnalysisDynamicSystems.DynamicSystems.Examples;

namespace AnalysisDynamicSystems.Calculations.Examples.Niding.Tests
{
    [TestClass()]
    public class NidingTests
    {
        [TestMethod()]
        public void GetResult_default_test()
        {
            // Arrange
            var ds = new DSTest();
            uint depth = 10;
            NidingCalculation niding = new(ds, depth);

            // Act
            var result = niding.GetResult();

            // Assert
            Assert.IsTrue(result == Math.Pow(2, depth) - 1);
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod()]
        public void GetResult_not_niding_test()
        {
            // Arrange
            Mock<DynamicSystem> ds = new Mock<DynamicSystem>();
            uint depth = 10;
            NidingCalculation niding = new(ds.Object, depth);

            // Act
            niding.GetResult();

            // Assert
        }

        [TestMethod()]
        public void GetResult_Lorenz_test()
        {
            // Arrange
            var ds = new LorenzDynamicSystems();
            uint depth = 10;
            NidingCalculation niding = new(ds, depth);
            niding.Steap = 0.001f;
            niding.CountIteration = 1_000_000;

            // Act
            var result = niding.GetResult();

            // Assert
            Assert.IsTrue(result == Math.Pow(2, depth - 1));
        }

        [TestMethod]
        public void GetResult_Niding_ShimizyMorioka_eq_18()
        {
            // Arrange
            ShimizyMoriokaDynamicSystem systems = new();
            NidingCalculation niding = new(systems, 5);
            systems[ShimizyMoriokaDynamicSystem.LAMBDA] = 0.8f;
            systems[ShimizyMoriokaDynamicSystem.ALPHA] = 0.5f;

            int actual = 18;

            // Act
            var result = niding.GetResult();

            Console.WriteLine(result);

            // Assert
            Assert.IsTrue(result == actual);
        }
    }
}