﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalysisDynamicSystems.DynamicSystems.Examples;

namespace AnalysisDynamicSystems.Calculations.Examples.Lyapynov.Tests
{
    [TestClass()]
    public class LyapynovCalculationTests
    {
        [TestMethod]
        public void Lyapynov_Lorenz_R_28_Test()
        {
            // Arrange
            LorenzDynamicSystems dynamicSystems = new();
            LyapynovCalculation calculation = new(dynamicSystems);
            dynamicSystems[LorenzDynamicSystems.SIGMA] = 10f;
            dynamicSystems[LorenzDynamicSystems.R] = 28f;
            dynamicSystems[LorenzDynamicSystems.B] = 8f/3;

            // Act
            var result = calculation.GetResult();
            // Assert
            Assert.IsTrue(result - 0.9 < 0.1);
        }

        [TestMethod]
        public void Lyapynov_Lorenz_R_20_Test()
        {
            // Arrange
            LorenzDynamicSystems dynamicSystems = new();
            LyapynovCalculation calculation = new(dynamicSystems);
            dynamicSystems[LorenzDynamicSystems.SIGMA] = 10f;
            dynamicSystems[LorenzDynamicSystems.R] = 20f;
            dynamicSystems[LorenzDynamicSystems.B] = 8f / 3;

            // Act
            var result = calculation.GetResult();
            // Assert
            Assert.IsTrue(result < 0f);
        }
    }
}