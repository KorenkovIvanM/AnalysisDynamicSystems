﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalysisDynamicSystems.Calculations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalysisDynamicSystems.Calculations.Examples;
using AnalysisDynamicSystemsTests;
using AnalysisDynamicSystems.Calculations.Examples.Attractor;

namespace AnalysisDynamicSystems.Calculations.Tests
{
    [TestClass]
    public class CalculationTests
    {
        [TestMethod]
        public void GetResult_Test_Dynamic_System_eq_E()
        {
            // Arrange
            var dSystem = new DSTest();
            var calculation = new DefaultAttractorCalculation(dSystem);

            calculation.CountIteration = 10_000;
            calculation.Steap = 1f/calculation.CountIteration;

            // Act

            var result = calculation.GetResult();

            // Assert
            Assert.IsTrue(Math.Abs(result[result.Length - 1].X - Math.E) < 0.01);

        }

        [TestMethod]
        public void GetResult_Test_Dynamic_have_System_eq_sqrt_E()
        {
            // Arrange
            var dSystem = new DSTest();
            var calculation = new DefaultAttractorCalculation(dSystem);

            calculation.CountIteration = 10_000;
            calculation.Steap = 1f / calculation.CountIteration;

            // Act

            var result = calculation.GetResult();

            // Assert
            Assert.IsTrue(Math.Abs(result[result.Length / 2].X - Math.Sqrt(Math.E)) < 0.01);

        }
    }
}