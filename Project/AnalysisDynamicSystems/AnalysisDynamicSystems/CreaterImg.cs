﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalysisDynamicSystems
{
    public class CreaterImg
    {
        private string WorkDirectory
        {
            get
            {
                string 
                    directoryWork = "D:\\\\img\\",
                    directoryHome = "C:\\\\Users\\user\\Pictures\\ADS\\";
                DirectoryInfo directoryInfo = new DirectoryInfo(directoryWork);

                if (directoryInfo.Exists)
                    return directoryWork;
                else
                    return directoryHome;
            }
        }
        public string Create(Color[,] colorsPixels, string NameImage)
        {
            if (colorsPixels == null) throw new Exception("Not matrix");

            NameImage = NameImage ?? $"C:\\\\Users\\user\\Pictures\\ADSimg_{DateTime.Now.ToString("dd_MM_yyyy_mm_ss")}.png";

            using Image<Rgba32> image = new(colorsPixels.GetLength(0), colorsPixels.GetLength(1));
            image.ProcessPixelRows(accessor =>
            {
                for (int y = 0; y < accessor.Height; y++)
                {
                    Span<Rgba32> pixelRow = accessor.GetRowSpan(y);
                    for (int x = 0; x < pixelRow.Length; x++)
                    {
                        ref Rgba32 pixel = ref pixelRow[x];
                        if (pixel.A == 0)
                            pixel = colorsPixels[x, y];
                    }
                }
            });

            image.Save(NameImage);

            return NameImage;
        }

        public string Create(Color[,] colorsPixels)
        {
            if (colorsPixels == null) throw new Exception("Not matrix");

            var NameImage = $"img_{DateTime.Now.ToString("dd_MM_yyyy_mm_ss")}.png";

            using Image<Rgba32> image = new(colorsPixels.GetLength(0), colorsPixels.GetLength(1));
            image.ProcessPixelRows(accessor =>
            {
                for (int y = 0; y < accessor.Height; y++)
                {
                    Span<Rgba32> pixelRow = accessor.GetRowSpan(y);
                    for (int x = 0; x < pixelRow.Length; x++)
                    {
                        ref Rgba32 pixel = ref pixelRow[x];
                        if (pixel.A == 0)
                            pixel = colorsPixels[x, y];
                    }
                }
            });

            image.Save(WorkDirectory + NameImage);

            return WorkDirectory + NameImage;
        }
    }
}
