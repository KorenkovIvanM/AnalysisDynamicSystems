﻿using AnalysisDynamicSystems.Linkers;

namespace AnalysisDynamicSystems.Visualisations.Examples
{
    public class ColorAttractorVisualisation : Visualisation<double>
    {
        public ColorAttractorVisualisation(ILinker<double> Linker) 
            : base(Linker) { }

        public override Color[,] GetColorImg(double[,] matrix)
        {
            var result = new Color[matrix.GetLength(0), matrix.GetLength(1)];

            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0;  j < matrix.GetLength(1); j++)
                {
                    result[i, j] = GetColor(matrix[i, j]);
                }
            }

            return result;
        }

        private static Color GetColor(double value)
        {
            if(double.IsNaN(value))
            {
                return Color.White;
            }
            else if(value > 0.001)
            {
                return Color.Red;
            }
            else if (value < -0.001)
            {
                return Color.Blue;
            }
            else
            {
                return Color.Yellow;
            }
        }
    }
}
