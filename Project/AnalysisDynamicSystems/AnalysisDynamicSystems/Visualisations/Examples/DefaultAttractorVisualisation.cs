﻿using AnalysisDynamicSystems.Linkers;

namespace AnalysisDynamicSystems.Visualisations.Examples
{
    public class DefaultAttractorVisualisation : Visualisation<sbyte>
    {
        public DefaultAttractorVisualisation(ILinker<sbyte> Linker) 
            : base(Linker) { }

        public override Color[,] GetColorImg(sbyte[,] matrix)
        {
            var result = new Color[Linker.Width, Linker.Height];

            for (int i = 0; i < Linker.Width; i++)
                for (int j = 0; j < Linker.Height; j++)
                    result[i, j] = matrix[i,j] == 0 ? Color.White: Color.Red;

            return result;
        }
    }
}
