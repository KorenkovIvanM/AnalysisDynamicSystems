﻿using System.Numerics;

namespace AnalysisDynamicSystems.Visualisations.Examples.NidingVisualisation
{
    public class NidingColorMap: IColorMap<uint>
    {
        SortedSet<uint> listValue = new SortedSet<uint>();
        Dictionary<uint, Color> listColor;

        public Color GetColor(uint value)
        {
            return listColor[value];
        }

        public void InitializationState(uint[,] data)
        {
            for (int i = 0; i < data.GetLength(0); i++)
                for (int j = 0; j < data.GetLength(1); j++)
                    listValue.Add(data[i, j]);

            listColor = new();
            Random random = new Random();
            int f = 0;
            foreach (var item in listValue)
            {
                f++;
                listColor[item] = new Color(
                    new Vector4(
                        (float)(listValue.Count - f) / listValue.Count,
                        (float)random.NextDouble(),
                        (float)f / listValue.Count,
                        (float)random.NextDouble()));
            }
        }
    }
}
