﻿using AnalysisDynamicSystems.Linkers;

namespace AnalysisDynamicSystems.Visualisations.Examples.NidingVisualisation
{
    public class DefaultNidingVisualisation : Visualisation<uint>
    {
        public DefaultNidingVisualisation(ILinker<uint> Linker) 
            : base(Linker) { }

        public override Color[,] GetColorImg(uint[,] matrix)
        {
            var colorBox = new NidingColorMap();
            colorBox.InitializationState(matrix);
            uint
                Width = (uint)matrix.GetLength(0),
                Height = (uint)matrix.GetLength(1);

            SortedSet<uint> listValues = new();
            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                    listValues.Add(matrix[i, j]);

            Color[,] result = new Color[Width, Height];

            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                    result[i, j] = colorBox.GetColor(matrix[i, j]);
            return result;
        }
    }
}
