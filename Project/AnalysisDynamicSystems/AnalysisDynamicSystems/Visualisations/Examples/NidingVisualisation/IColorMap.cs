﻿namespace AnalysisDynamicSystems.Visualisations.Examples.NidingVisualisation
{
    public interface IColorMap<T>
    {
        void InitializationState(T[,] data);
        Color GetColor(T value);
    }
}
