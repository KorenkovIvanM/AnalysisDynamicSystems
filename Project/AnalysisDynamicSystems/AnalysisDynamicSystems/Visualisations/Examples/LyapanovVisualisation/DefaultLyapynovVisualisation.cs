﻿using AnalysisDynamicSystems.Linkers;

namespace AnalysisDynamicSystems.Visualisations.Examples.LyapanovVisualisation
{
    public class DefaultLyapynovVisualisation : Visualisation<float>
    {
        public DefaultLyapynovVisualisation(ILinker<float> Linker)
            : base(Linker) { }

        public override Color[,] GetColorImg(float[,] matrix)
        {
            Color[,] result = new Color[matrix.GetLength(0), matrix.GetLength(1)];

            for (int i = 0; i < result.GetLength(0); i++)
                for (int j = 0; j < result.GetLength(1); j++)
                    result[i, j] = matrix[i, j] < 0.05 ? Color.Aqua : Color.Red;

            return result;
        }
    }
}
