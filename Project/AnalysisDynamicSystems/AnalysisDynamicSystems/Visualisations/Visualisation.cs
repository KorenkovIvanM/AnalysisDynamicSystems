﻿using AnalysisDynamicSystems.Calculations;
using AnalysisDynamicSystems.Linkers;

namespace AnalysisDynamicSystems.Visualisations
{
    public abstract class Visualisation<TResult>
    {
        public ILinker<TResult> Linker { get; set; }

        public Visualisation(ILinker<TResult> Linker)
        {
            this.Linker = Linker;
        }
        public Color[,] GetColorImg() => GetColorImg(Linker.GetMatrix());
        public abstract Color[,] GetColorImg(TResult[,] matrix);

    }
}
