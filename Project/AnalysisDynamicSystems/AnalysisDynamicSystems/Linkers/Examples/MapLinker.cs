﻿using AnalysisDynamicSystems.Calculations;
using AnalysisDynamicSystems.Models;

namespace AnalysisDynamicSystems.Linkers.Examples
{
    public class MapLinker : Linker<uint, uint>
    {
        
        public event Saver<uint> onSave;
        public Map CurrentMap => currentMap;
        private Map currentMap;
        public MapLinker(uint Width, uint Height, Calculation<uint> Calculation)
            : base(Width, Height, Calculation)
        {
            currentMap = new Map();
            currentMap.Width = Width;
            currentMap.Height = Height;
            currentMap.Calculation = nameof(Calculation);
            currentMap.DynamicSystem = nameof(Calculation.CurrentDynamicSystems);
            currentMap.Name = $"DS_{currentMap.DynamicSystem}_C{currentMap.Calculation}_{DateTime.Now.GetHashCode()}";
        }
        public MapLinker(Map currentMap, Calculation<uint> Calculation)
            :base(currentMap.Width, currentMap.Height, Calculation)
        {
            this.currentMap = currentMap;
        }

        public override uint[,] GetMatrix()
        {
            var result = new uint[Width, Height];

            Parallel.For(0, Width, indexWidth =>
            {
                var _currentCalculation = (Calculation<uint>)CurrentCalculation.Clone();
                var currentResult = new uint[Width];
                _currentCalculation.CurrentDynamicSystems[CurrentMap.WidthParametrName] =
                    GetParametrMap((uint)indexWidth, Width, CurrentMap.WidthParametrBegin, CurrentMap.WidthParametrEnd);
                for (var indexHeight = 0; indexHeight < Height; indexHeight++)
                {
                    _currentCalculation.CurrentDynamicSystems[CurrentMap.HeightParametrName] =
                        GetParametrMap((uint)indexHeight, Height, CurrentMap.HeightParametrBegin, CurrentMap.HeightParametrEnd);
                    result[indexWidth, indexHeight] = _currentCalculation.GetResult();
                    currentResult[indexHeight] = result[indexWidth, indexHeight];
                }
                onSave?.Invoke((uint)indexWidth, ref currentMap, ref currentResult);
            });
            
            return result;
        }

        private float GetParametrMap(uint index, uint size, float StartParametr, float EndParametr)
        {
            return (EndParametr - StartParametr) / size * index + StartParametr;
        }
    }
}
