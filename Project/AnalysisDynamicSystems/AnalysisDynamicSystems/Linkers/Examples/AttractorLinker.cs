﻿using AnalysisDynamicSystems.Calculations;
using AnalysisDynamicSystems.Calculations.Examples.Attractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalysisDynamicSystems.Linkers.Examples
{
    public class AttractorLinker : Linker<Vector3[], sbyte>
    {
        public new AttractorCalculation CurrentCalculation { get; set; }

        public AttractorLinker(uint Width, uint Height, AttractorCalculation calculation) 
            : base(Width, Height, calculation) 
        {
            CurrentCalculation = calculation;
        }

        public override sbyte[,] GetMatrix()
        {
            var result = new sbyte[Width, Height];
            uint indexWidth, indexHeight;
            var data = CurrentCalculation.GetResult();

            for(int i = 0; i < data.Length; i++)
            {
                indexWidth = Convert.ToUInt32(Math.Round((data[i].X - CurrentCalculation.MinX) / (CurrentCalculation.MaxX - CurrentCalculation.MinX) * Width));
                indexHeight = Convert.ToUInt32(Math.Round((data[i].Z - CurrentCalculation.MinZ) / (CurrentCalculation.MaxZ - CurrentCalculation.MinZ) * Height));

                if(
                    0 <= indexWidth && indexWidth < Width &&
                    0 <= indexHeight && indexHeight < Height)
                    result[indexWidth, indexHeight] = 1;
            }

            return result;
        }
    }
}
