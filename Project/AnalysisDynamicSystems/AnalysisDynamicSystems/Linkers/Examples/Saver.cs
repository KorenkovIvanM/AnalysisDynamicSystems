﻿using AnalysisDynamicSystems.Models;

namespace AnalysisDynamicSystems.Linkers.Examples
{
    public delegate void Saver<T>(uint index, ref Map map, ref T[] result);
}
