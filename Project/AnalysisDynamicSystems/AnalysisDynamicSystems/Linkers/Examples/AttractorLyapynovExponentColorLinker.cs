﻿using AnalysisDynamicSystems.Calculations;
using AnalysisDynamicSystems.Calculations.Examples.Attractor;
using System.Numerics;

namespace AnalysisDynamicSystems.Linkers.Examples
{
    public class AttractorLyapynovExponentColorLinker : Linker<(Vector3[] trajectory, double[] values), double>
    {
        public new GenericAttractorCalculation<(Vector3[] trajectory, double[] values)> CurrentCalculation { get; set; }

        public AttractorLyapynovExponentColorLinker(uint Width, uint Height, GenericAttractorCalculation<(Vector3[] trajectory, double[] values)> calculation)
            : base(Width, Height, calculation)
        {
            CurrentCalculation = calculation;
        }

        public override double[,] GetMatrix()
        {
            var result = new double[Width, Height];

            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                    result[i, j] = double.NaN;

            uint indexWidth, indexHeight;
            var data = CurrentCalculation.GetResult();

            for (int i = 0; i < data.trajectory.Length; i++)
            {
                indexWidth = Convert.ToUInt32(Math.Round((data.trajectory[i].X - CurrentCalculation.MinX) / (CurrentCalculation.MaxX - CurrentCalculation.MinX) * Width));
                indexHeight = Convert.ToUInt32(Math.Round((data.trajectory[i].Z - CurrentCalculation.MinZ) / (CurrentCalculation.MaxZ - CurrentCalculation.MinZ) * Height));

                if (
                    0 <= indexWidth && indexWidth < Width &&
                    0 <= indexHeight && indexHeight < Height)
                    result[indexWidth, indexHeight] = data.values[i];
            }

            return result;
        }
    }
}
