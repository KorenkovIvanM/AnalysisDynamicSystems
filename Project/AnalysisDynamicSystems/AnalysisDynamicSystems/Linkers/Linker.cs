﻿using AnalysisDynamicSystems.Calculations;

namespace AnalysisDynamicSystems.Linkers
{
    public abstract class Linker<TCalculation, TResult>: ILinker<TResult>
    {
        public uint Width { get; set; }
        public uint Height { get; set; }

        public Linker(uint Width, uint Height, Calculation<TCalculation> Calculation)
        {
            this.Width = Width;
            this.Height = Height;
            this.CurrentCalculation = Calculation;
        }

        public Calculation<TCalculation> CurrentCalculation { get; set; }
        public abstract TResult[,] GetMatrix();
    }
}
