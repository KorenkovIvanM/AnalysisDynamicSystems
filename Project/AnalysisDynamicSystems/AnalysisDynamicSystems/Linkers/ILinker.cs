﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalysisDynamicSystems.Linkers
{
    public interface ILinker<T>
    {
        uint Width { get; set; }
        uint Height { get; set; }
        T[,] GetMatrix();
    }
}
