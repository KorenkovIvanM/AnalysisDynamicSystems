﻿using AnalysisDynamicSystems.DynamicSystems;

namespace AnalysisDynamicSystems.Calculations
{
    public abstract class Calculation<TCalculaion>: ICloneable
    {
        public uint CountIteration { get; set; } = 1_000_000;
        public float Steap { get; set; } = 0.001f;
        public DynamicSystem CurrentDynamicSystems { get; set; }
        public Calculation(DynamicSystem dynamicSystem)
        {
            CurrentDynamicSystems = dynamicSystem;
        }
        public abstract TCalculaion GetResult();

        public object Clone()
        {
            var result = MemberwiseClone();
            ((Calculation<TCalculaion>)result).CurrentDynamicSystems = (DynamicSystem)CurrentDynamicSystems.Clone();
            return result;
        }
    }
}
