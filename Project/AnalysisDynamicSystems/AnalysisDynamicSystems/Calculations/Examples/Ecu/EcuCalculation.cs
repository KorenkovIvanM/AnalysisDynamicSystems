﻿using AnalysisDynamicSystems.DynamicSystems;
using System.Numerics;

namespace AnalysisDynamicSystems.Calculations.Examples.Ecu
{
    public class EcuCalculation : Calculation<(Vector3 position, Vector3 direction)[]>
    {
        public float Epsilon { get; set; } = 0.01f;
        public EcuCalculation(DynamicSystem dynamicSystem) 
            : base(dynamicSystem) { }

        /// <summary>
        /// Метод возврашает набор
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override (Vector3 position, Vector3 direction)[] GetResult()
        {
            var result = new (Vector3 position, Vector3 direction)[CountIteration];

            Vector3
                startVector = CurrentDynamicSystems.GetDefaultStartVector(),
                closeVector = startVector with { Z = startVector.Z + Epsilon };

            for(int i = 0; i < result.Length; i++)
            {
                startVector = CurrentDynamicSystems.GetNextVector(startVector, Steap);
                closeVector = CurrentDynamicSystems.GetNextVector(closeVector, Steap);
            }

            return result;
        }
    }
}
