﻿using AnalysisDynamicSystems.DynamicSystems;
using System.Numerics;

namespace AnalysisDynamicSystems.Calculations.Examples.Attractor
{
    public class DefaultAttractorCalculation: AttractorCalculation
    {
        public DefaultAttractorCalculation(DynamicSystem dynamicSystem) 
            : base(dynamicSystem) { }

        public override Vector3[] GetResult()
        {
            var result = new Vector3[CountIteration];

            Vector3 vector = CurrentDynamicSystems.GetDefaultStartVector();
            result[0] = vector;

            for (int i = 1; i < CountIteration; i++)
            {
                result[i] = CurrentDynamicSystems.GetNextVector(result[i - 1], Steap);
                SetBoxExist(result[i]);
            }

            return result;
        }
    }
}
