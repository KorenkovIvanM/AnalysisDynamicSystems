﻿using AnalysisDynamicSystems.DynamicSystems;
using System.Numerics;
using System.Runtime.Intrinsics;

namespace AnalysisDynamicSystems.Calculations.Examples.Attractor
{
    public abstract class AttractorCalculation : Calculation<Vector3[]>
    {
        protected AttractorCalculation(DynamicSystem dynamicSystem) 
            : base(dynamicSystem) { }

        public float MaxX { get; private set; } = float.MinValue;
        public float MaxY { get; private set; } = float.MinValue;
        public float MaxZ { get; private set; } = float.MinValue;
        public float MinX { get; private set; } = float.MaxValue;
        public float MinY { get; private set; } = float.MaxValue;
        public float MinZ { get; private set; } = float.MaxValue;
        public uint CountIterationBox { get; set; } = 10_000;
        public float SteapBox { get; set; } = 0.1f;

        public void SetBoxExistLongTime()
        {
            Vector3 vector = CurrentDynamicSystems.GetDefaultStartVector();

            for(int i = 0; i < CountIterationBox; i++)
            {
                SetBoxExist(vector);
                vector = CurrentDynamicSystems.GetNextVector(vector, SteapBox);
            }
        }

        public void SetBoxExist(Vector3 vector)
        {
            if (vector.X < MinX) MinX = vector.X;
            if (vector.Y < MinY) MinY = vector.Y;
            if (vector.Z < MinZ) MinZ = vector.Z;
            if (vector.X > MaxX) MaxX = vector.X;
            if (vector.Y > MaxY) MaxY = vector.Y;
            if (vector.Z > MaxZ) MaxZ = vector.Z;
        }
    }
}
