﻿using AnalysisDynamicSystems.Calculations.Examples.Attractor;
using AnalysisDynamicSystems.DynamicSystems;
using System.Numerics;

namespace AnalysisDynamicSystems.Calculations.Examples.AttractorLyapynovExponentColor
{
    public class AttractorLyapynovExponentColorCalculation : GenericAttractorCalculation<(Vector3[] trajectory, double[] values)>
    {
        public float Eps { get; set; } = 0.01f;
        public AttractorLyapynovExponentColorCalculation(DynamicSystem dynamicSystem) 
            : base(dynamicSystem) { }

        public override (Vector3[] trajectory, double[] values) GetResult()
        {
            Vector3[]
                resultTrajectory = new Vector3[CountIteration];
            double[]
                resultValues = new double[CountIteration];
            Vector3
                startVector = CurrentDynamicSystems.GetDefaultStartVector(),
                closeVector = startVector with { Z = startVector.Z + Eps };

            for (int i = 0; i < CountIteration; i++)
            {
                startVector = CurrentDynamicSystems.GetNextVector(startVector, Steap);
                closeVector = CurrentDynamicSystems.GetNextVector(closeVector, Steap);

                var lengthSeparet = (startVector - closeVector).Length();

                resultTrajectory[i] = startVector;
                resultValues[i] += Math.Log(lengthSeparet / Eps);


                closeVector.X = startVector.X + (closeVector.X - startVector.X) / lengthSeparet * Eps;
                closeVector.Y = startVector.Y + (closeVector.Y - startVector.Y) / lengthSeparet * Eps;
                closeVector.Z = startVector.Z + (closeVector.Z - startVector.Z) / lengthSeparet * Eps;

                SetBoxExist(resultTrajectory[i]);
            }

            return (resultTrajectory, resultValues);
        }
    }
}
