﻿using AnalysisDynamicSystems.DynamicSystems;
using System.Numerics;

namespace AnalysisDynamicSystems.Calculations.Examples.Lyapynov
{
    internal class OneLyapynovCalculation : Calculation<List<double>>
    {
        public float Eps { get; set; } = 0.01f;
        public OneLyapynovCalculation(DynamicSystem dynamicSystem) 
            : base(dynamicSystem) { }

        public override List<double> GetResult()
        {
            List<double>
                result = new();
            Vector3
                startVector = CurrentDynamicSystems.GetDefaultStartVector(),
                closeVector = startVector with { Z = startVector.Z + Eps };
            double
                buff = 0;

            for (int i = 0; i < CountIteration; i++)
            {
                startVector = CurrentDynamicSystems.GetNextVector(startVector, Steap);
                closeVector = CurrentDynamicSystems.GetNextVector(closeVector, Steap);

                var lengthSeparet = (startVector - closeVector).Length();

                buff += Math.Log(lengthSeparet / Eps);

                result.Add(buff / (i * Steap));

                closeVector.X = startVector.X + (closeVector.X - startVector.X) / lengthSeparet * Eps;
                closeVector.Y = startVector.Y + (closeVector.Y - startVector.Y) / lengthSeparet * Eps;
                closeVector.Z = startVector.Z + (closeVector.Z - startVector.Z) / lengthSeparet * Eps;
            }

            return result;
        }
    }
}
