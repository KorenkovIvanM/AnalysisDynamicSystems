﻿using AnalysisDynamicSystems.DynamicSystems;
using System.Numerics;

namespace AnalysisDynamicSystems.Calculations.Examples.Lyapynov
{
    public class LyapynovCalculation : Calculation<float>
    {
        public float Eps { get; set; } = 0.01f;
        public LyapynovCalculation(DynamicSystem dynamicSystem) 
            : base(dynamicSystem) { }

        public override float GetResult()
        {
            double 
                result = 0;
            Vector3
                startVector = CurrentDynamicSystems.GetDefaultStartVector(),
                closeVector = startVector with { Z = startVector.Z + Eps };

            for (int i = 0; i < CountIteration; i++)
            {
                startVector = CurrentDynamicSystems.GetNextVector(startVector, Steap);
                closeVector = CurrentDynamicSystems.GetNextVector(closeVector, Steap);

                var lengthSeparet = (startVector - closeVector).Length();

                result += Math.Log(lengthSeparet / Eps);

                if (result == double.PositiveInfinity || result == double.NegativeInfinity || result == double.NaN) return float.NaN;

                closeVector.X = startVector.X + (closeVector.X - startVector.X) / lengthSeparet * Eps;
                closeVector.Y = startVector.Y + (closeVector.Y - startVector.Y) / lengthSeparet * Eps;
                closeVector.Z = startVector.Z + (closeVector.Z - startVector.Z) / lengthSeparet * Eps;
            }

            return (float)(result / (CountIteration * Steap));
        }
    }
}
