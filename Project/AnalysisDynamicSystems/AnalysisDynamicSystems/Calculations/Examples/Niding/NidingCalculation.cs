﻿using AnalysisDynamicSystems.DynamicSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalysisDynamicSystems.Calculations.Examples.Niding
{
    public class NidingCalculation : Calculation<uint>
    {
        private const string
            MESSAGE_ERROR_NIDING = "System {0} not realisation INiding interface";
        public readonly uint
            Depth;

        public NidingCalculation(DynamicSystem dynamicSystem, uint depth) 
            : base(dynamicSystem) 
        { 
            Depth = depth;
        }

        public override uint GetResult()
        {
            if (CurrentDynamicSystems is not INidingDynamicSystem) 
                throw new ArgumentException(MESSAGE_ERROR_NIDING);

            var system = CurrentDynamicSystems as INidingDynamicSystem;
            var vector = CurrentDynamicSystems.GetDefaultStartVector();
            Vector3 buff;
            uint
                result = 0,
                countCurrentDepth = 0;

            for(int i = 0; i < CountIteration; i++)
            {
                buff = vector;
                vector = CurrentDynamicSystems.GetNextVector(vector, Steap);
                if(system.IsCritc(buff, vector))
                {
                    result <<= 1;
                    result += system.GetCurrentVector(vector);
                    countCurrentDepth++;
                    if(countCurrentDepth == Depth) 
                        break;
                }
            }

            return result;
        }
    }
}
