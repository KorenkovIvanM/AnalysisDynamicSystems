﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalysisDynamicSystems.Calculations.Examples.Niding
{
    public interface INidingDynamicSystem
    {
        bool IsCritc(Vector3 vBegin, Vector3 vEnd);
        byte GetCurrentVector(Vector3 currentVector);
    }
}
