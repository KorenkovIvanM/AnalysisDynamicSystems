﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalysisDynamicSystems.Models
{
    public class MapValue
    {
        public int Id { get; set; }
        public int MapId { get; set; }
        public string Value { get; set; } = "0";
        public uint IndexWidth { get; set; }
        public uint IndexHeight { get; set; }
    }
}
