﻿namespace AnalysisDynamicSystems.Models
{
    public class Map
    {
        public int Id { get; set; }
        public uint Width { get; set; }
        public uint Height { get; set; }
        public string Name { get; set; } = null!;
        public string DynamicSystem { get; set; } = null!;
        public string Calculation { get; set; } = null!;
        public string WidthParametrName { get; set; } = null!;
        public string HeightParametrName { get; set; } = null!;
        public float WidthParametrBegin { get; set; }
        public float HeightParametrBegin { get; set; }
        public float WidthParametrEnd { get; set; }
        public float HeightParametrEnd { get; set; }
    }
}
