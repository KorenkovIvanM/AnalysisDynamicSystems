﻿using AnalysisDynamicSystems.Calculations.Examples.Niding;
using System.Numerics;

namespace AnalysisDynamicSystems.DynamicSystems.Examples
{
    public class ShimizyMoriokaDynamicSystem : DynamicSystem, INidingDynamicSystem
    {
        public const string
            LAMBDA = "LAMBDA",
            ALPHA = "ALPHA";
        public const float
            DEFAULT_VALUE_LAMBDA = 1f,
            DEFAULT_VALUE_ALPHA = 0.8f;

        public override float Fx(Vector3 vector) => vector.Y;
        public override float Fy(Vector3 vector) => vector.X - this[LAMBDA] * vector.Y - vector.X * vector.Z;
        public override float Fz(Vector3 vector) => -this[ALPHA] * vector.Z + vector.X * vector.X;

        public byte GetCurrentVector(Vector3 currentVector) => currentVector.X > 0 ? (byte)1 : (byte)0;

        public override Dictionary<string, float> GetDefaultParametrs()
            => new Dictionary<string, float>()
            {
                { LAMBDA, DEFAULT_VALUE_LAMBDA },
                { ALPHA, DEFAULT_VALUE_ALPHA },
            };

        public override Vector3 GetDefaultStartVector()
            => new Vector3() { X = 0.01f, Y = 0, Z = 0 };

        
        public bool IsCritc(Vector3 vBegin, Vector3 vEnd)
        {
            if (Math.Abs(vBegin.X) < Math.Sqrt(this[ALPHA])) return false;
            return (Fx(vBegin) * Fx(vEnd)) < 0;
        }
    }
}
