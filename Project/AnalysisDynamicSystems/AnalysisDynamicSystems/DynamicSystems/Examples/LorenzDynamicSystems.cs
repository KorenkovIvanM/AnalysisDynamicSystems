﻿using AnalysisDynamicSystems.Calculations.Examples.Niding;
using System.Numerics;

namespace AnalysisDynamicSystems.DynamicSystems.Examples
{
    public class LorenzDynamicSystems : DynamicSystem, INidingDynamicSystem
    {
        public const string 
            SIGMA  = "Sibma", 
            R = "R", 
            B = "B";
        public const float
            DEFAULT_VALUE_SIGMS = 10f, 
            DEFAULT_VALUE_R = 28f, 
            DEFAULT_VALUE_B = 8f/3f;

        public override float Fx(Vector3 vector) => this[SIGMA] * (vector.Y - vector.X);
        public override float Fy(Vector3 vector) => vector.X * (this[R] - vector.Z) - vector.Y; 
        public override float Fz(Vector3 vector) => vector.X * vector.Y - this[B] * vector.Z;

        public byte GetCurrentVector(Vector3 currentVector) => currentVector.X > 0 ? (byte)1 : (byte)0;

        public override Dictionary<string, float> GetDefaultParametrs()
            => new Dictionary<string, float>()
            {
                { SIGMA, DEFAULT_VALUE_SIGMS },
                { R, DEFAULT_VALUE_R },
                { B, DEFAULT_VALUE_B },
            };

        public override Vector3 GetDefaultStartVector() 
            => new Vector3() { X= 0.01f, Y = 0, Z = 0 };

        public bool IsCritc(Vector3 vBegin, Vector3 bEnd)
        {
            if (vBegin.X > Math.Sqrt(this[B] * (this[R] - 1)) && vBegin.Y > Math.Sqrt(this[B] * (this[R] - 1))) return false;
            return Fy(vBegin) * Fy(bEnd) < 0;
        }
    }
}
