﻿using AnalysisDynamicSystems.Calculations;
using System.Numerics;

namespace AnalysisDynamicSystems.DynamicSystems
{
    public abstract class DynamicSystem: ICloneable
    {
        #region Fileds
        private const string
            MESSAGE_ERROR_NON_VALID_STEAP = "Значение Steap: {0} не валидно.",
            DEFAULT_DYNAMIC_SYSTEMS_NAME = "Not name dynamic systems";
        protected Dictionary<string, float> Parametrs;
        #endregion
        #region Indexator
        public float this[string NameParametr] 
        {
            get => Parametrs[NameParametr];
            set { Parametrs[NameParametr] = value; }
        }
        #endregion
        #region Propertyse
        public string Name => ToString() ?? DEFAULT_DYNAMIC_SYSTEMS_NAME;
        #endregion
        #region Methods
        #region Systems
        /// <summary>
        /// Частная производная ДС по X (dx/dt)
        /// </summary>
        /// <param name="vector">Текуший вектор в системе</param>
        /// <returns>Частная производная ДС по (dx/dt)</returns>
        public abstract float Fx(Vector3 vector);
        /// <summary>
        /// Частная производная ДС по Y (dy/dt)
        /// </summary>
        /// <param name="vector">Текуший вектор в системе</param>
        /// <returns>Частная производная ДС по (dy/dt)</returns>
        public abstract float Fy(Vector3 vector);
        /// <summary>
        /// Частная производная ДС по Z (dz/dt)
        /// </summary>
        /// <param name="vector">Текуший вектор в системе</param>
        /// <returns>Частная производная ДС по (dz/dt)</returns>
        public abstract float Fz(Vector3 vector);
        #endregion
        /// <summary>
        /// Возврашает дефолтное значение для старта
        /// </summary>
        /// <returns>Вектор</returns>
        public abstract Vector3 GetDefaultStartVector();
        /// <summary>
        /// Возврашает колекцию параметров для данной системы
        /// </summary>
        /// <returns>колекция параметров</returns>
        public abstract Dictionary<string, float> GetDefaultParametrs();
        /// <summary>
        /// Возврашает список параметров
        /// </summary>
        /// <returns>Список параметров</returns>
        public List<string> GetListParametrs()
            => GetDefaultParametrs().Select(item => item.Key).ToList();
        /// <summary>
        /// Метод численного интегрирования Рунге-Кутты 4-го порядка
        /// </summary>
        /// <param name="vector">Стартовое значение</param>
        /// <param name="steap">Шаг интегрирования</param>
        /// <returns>Результат численного интегрирования</returns>
        /// <exception cref="ArgumentException">При не корректном значение шага интегрирования 0.00000001 < steap < 10</exception>
        public Vector3 GetNextVector(Vector3 vector, float steap)
        {
            if (
                float.IsNaN(steap) ||
                float.IsInfinity(steap) ||
                steap >= 10 ||
                steap < 0.00000001f)
                throw new ArgumentException(string.Format(MESSAGE_ERROR_NON_VALID_STEAP, steap));

            float[,] k = new float[4, 3];

            k[0, 0] = Fx(vector);
            k[0, 1] = Fy(vector);
            k[0, 2] = Fz(vector);

            k[1, 0] = Fx(new Vector3(vector.X + steap / 2, vector.Y + steap / 2 * k[0, 1], vector.Z + steap / 2 * k[0, 2]));
            k[1, 1] = Fy(new Vector3(vector.X + steap / 2 * k[0, 0], vector.Y + steap / 2, vector.Z + steap / 2 * k[0, 2]));
            k[1, 2] = Fz(new Vector3(vector.X + steap / 2 * k[0, 0], vector.Y + steap / 2 * k[0, 1], vector.Z + steap / 2));

            k[2, 0] = Fx(new Vector3(vector.X + steap / 2, vector.Y + steap / 2 * k[1, 1], vector.Z + steap / 2 * k[1, 2]));
            k[2, 1] = Fy(new Vector3(vector.X + steap / 2 * k[1, 0], vector.Y + steap / 2, vector.Z + steap / 2 * k[1, 2]));
            k[2, 2] = Fz(new Vector3(vector.X + steap / 2 * k[1, 0], vector.Y + steap / 2 * k[1, 1], vector.Z + steap / 2));

            k[3, 0] = Fx(new Vector3(vector.X + steap, vector.Y + steap * k[2, 1], vector.Z + steap * k[2, 2]));
            k[3, 1] = Fy(new Vector3(vector.X + steap * k[2, 0], vector.Y + steap, vector.Z + steap * k[2, 2]));
            k[3, 2] = Fz(new Vector3(vector.X + steap * k[2, 0], vector.Y + steap * k[2, 1], vector.Z + steap));

            vector.X += steap / 6 * (k[0, 0] + 2 * k[1, 0] + 2 * k[2, 0] + k[3, 0]);
            vector.Y += steap / 6 * (k[0, 1] + 2 * k[1, 1] + 2 * k[2, 1] + k[3, 1]);
            vector.Z += steap / 6 * (k[0, 2] + 2 * k[1, 2] + 2 * k[2, 2] + k[3, 2]);

            return vector;
        }

        public object Clone()
        {
            var result = MemberwiseClone();
            ((DynamicSystem)result).Parametrs = GetDefaultParametrs();
            return result;
        }
        #endregion
        #region ctor
        public DynamicSystem()
        {
            Parametrs = GetDefaultParametrs();
        }
        #endregion
    }
}
