﻿using AnalysisDynamicSystems.Calculations;
using AnalysisDynamicSystems.DynamicSystems;
using AnalysisDynamicSystems.Linkers;
using AnalysisDynamicSystems.Visualisations;

namespace AnalysisDynamicSystems
{
    /// <summary>
    /// Класс для работы с динамическими системами и математическими эксперементами.
    /// </summary>
    public class DynamicSystemCalculation<TCalculation, TResult>
    {
        private DynamicSystem _dynamicSystem;
        private Calculation<TCalculation> _calculation;
        private Linker<TCalculation, TResult> _linker;
        private Visualisation<TResult> _visualisation;

        /// <summary>
        /// Создаёт объект DynamicSystemCalculation.
        /// </summary>
        /// <param name="dynamicSystem">Динамическая система</param>
        /// <param name="calculation">Тип вычислений</param>
        /// <param name="linker">Компоновшик</param>
        /// <param name="visualisation">Визуализатор</param>
        public DynamicSystemCalculation(
            DynamicSystem dynamicSystem,
            Calculation<TCalculation> calculation,
            Linker<TCalculation, TResult> linker,
            Visualisation<TResult> visualisation)
        {
            _dynamicSystem = dynamicSystem;
            _calculation = calculation;
            _linker = linker;
            _visualisation = visualisation;

            calculation.CurrentDynamicSystems = dynamicSystem;
            linker.CurrentCalculation = calculation;
        }

        public Color[,] GetResult() => _visualisation.GetColorImg(_linker.GetMatrix());
    }
}
