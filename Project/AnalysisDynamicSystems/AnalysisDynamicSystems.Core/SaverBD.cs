﻿using AnalysisDynamicSystems.Core.MainContext;
using AnalysisDynamicSystems.Models;

namespace AnalysisDynamicSystems.Core
{
    public static class SaverBD
    {
        public static void Saver<T>(uint index, ref Map map, ref T[] values)
        {
            var mapId = map.Id;
            var indexHeight = 0;
            using(Context context = new())
            {
                context.MapValues.AddRange(
                    values.Select(item => new MapValue()
                    { 
                        MapId = mapId,
                        Value = item?.ToString() ?? "NAN",
                        IndexWidth = index,
                        IndexHeight = (uint)indexHeight++,
                    }));
                context.SaveChanges();
            }
        }

        public static void WriteMap(Map map)
        {
            using(Context context = new())
            {
                context.Maps.Add(map);
                context.SaveChanges();
            }
        }
    }
}
