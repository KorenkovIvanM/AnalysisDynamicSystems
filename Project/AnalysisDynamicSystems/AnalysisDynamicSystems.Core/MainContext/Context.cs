﻿using AnalysisDynamicSystems.Models;
using Microsoft.EntityFrameworkCore;

namespace AnalysisDynamicSystems.Core.MainContext
{
    public class Context: DbContext
    {
        public DbSet<Map> Maps { get; set; }    
        public DbSet<MapValue> MapValues { get; set; }

        public Context() => Database.EnsureCreated();
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlite("data source=C:\\Users\\user\\Documents\\bd\\test.db");
    }
}
