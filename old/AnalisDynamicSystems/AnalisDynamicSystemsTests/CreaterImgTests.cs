﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SixLabors.ImageSharp;
using AnalisDynamicSystems.ConcritDynamicSystems;
using AnalisDynamicSystems.ConcritCalculation.Attractor;
using AnalisDynamicSystems.ConcritCalculation.Lyapynov;
using AnalisDynamicSystems.ConcritCalculation.Niding;
using AnalisDynamicSystems.ConcritVisualisation.Attractor;
using AnalisDynamicSystems.ConcritVisualisation.Map;
using AnalisDynamicSystems.ConcritVisualisation.Map.ColorMap;
using AnalisDynamicSystems.ConcritVisualisation.Diagramm;

namespace AnalisDynamicSystems.Tests
{
    [TestClass]
    public class CreaterImgTests
    {
        [TestMethod]
        public void CreateImg_Positiv_Test()
        {
            // Arrange
            CreaterImg creater = new();
            FileInfo file;
            int Size = 100;
            Color[,] img = new Color[Size, Size];
            for (int i = 0; i < Size; i++)
                for (int j = 0; j < Size; j++)
                    img[i, j] = i < j ? Color.Aqua : Color.Beige;

            // Act
            file = new(creater.Create(img));

            // Assert
            Assert.IsTrue(file.Exists);
        }

        [TestMethod]
        public void Create_Lorenz_Test()
        {
            // Arrange
            CreaterImg creater = new();
            FileInfo file;
            LorenzSystems systems = new();
            PastAttractorCalculation calculation = new(systems);
            AttractorVisualisation visualisation = new(calculation, "my default name", DWeight: NameDimension.Z);

            // Act
            file = new(creater.Create(visualisation));

            // Assert
            Assert.IsTrue(file.Exists);
        }

        [TestMethod]
        public void Create_ShimizyMorioka_Test()
        {
            // Arrange
            CreaterImg creater = new();
            FileInfo file;
            ShimizyMoriokaDynamicSystem systems = new();
            PastAttractorCalculation calculation = new(systems);
            calculation[ShimizyMoriokaDynamicSystem.LAMBDA] = 0.8f;
            calculation[ShimizyMoriokaDynamicSystem.ALPHA] = 0.7f;
            AttractorVisualisation visualisation = new(calculation, "my default name", DWeight: NameDimension.Z);

            // Act
            file = new(creater.Create(visualisation));

            // Assert
            Assert.IsTrue(file.Exists);
        }

        [TestMethod]
        public void Lyapynov_Lorenz_R_28_Test()
        {
            // Arrange
            LorenzSystems systems = new();
            LyapynovCalculation lyapynov = new(systems);
            lyapynov[LorenzSystems.R] = 28;

            // Act
            var result = lyapynov.Act();

            Console.WriteLine(result);

            // Assert
            Assert.IsTrue(result - 0.9 < 0.1);
        }

        [TestMethod]
        public void Lyapynov_Lorenz_R_20_Test()
        {
            // Arrange
            LorenzSystems systems = new();
            LyapynovCalculation lyapynov = new(systems);
            lyapynov[LorenzSystems.R] = 20;

            // Act
            var result = lyapynov.Act();

            Console.WriteLine(result);

            // Assert
            Assert.IsTrue(result < 0);
        }

        [TestMethod]
        public void Niding()
        {
            // Arrange
            ShimizyMoriokaDynamicSystem systems = new();
            NidingCalculation niding = new(systems, 5);
            niding[ShimizyMoriokaDynamicSystem.LAMBDA] = 0.8f;
            niding[ShimizyMoriokaDynamicSystem.ALPHA] = 0.5f;

            int actual = 18;

            // Act
            var result = niding.Act();

            Console.WriteLine(result);

            // Assert
            Assert.IsTrue(result == actual);
        }

        [TestMethod]
        public void Map_Creater_Success()
        {
            // Arrange
            CreaterImg creater = new();
            ShimizyMoriokaDynamicSystem systems = new();
            NidingCalculation niding = new(systems, 5);
            NidintColorMap colorMap = new NidintColorMap();
            MapVisualisation<int> visualisation = new(
                niding, 
                "Default name", 
                colorMap, 
                ShimizyMoriokaDynamicSystem.LAMBDA, 
                ShimizyMoriokaDynamicSystem.ALPHA,
                0,0,2,2, Convert.ToInt32);
            FileInfo file;
            // Act
            file = new(creater.Create(visualisation));

            // Assert
            Assert.IsTrue(file.Exists);
        }

        [TestMethod]
        public void Map_Lyapynov_Creater_Success()
        {
            // Arrange
            CreaterImg creater = new();
            ShimizyMoriokaDynamicSystem systems = new();
            LyapynovCalculation lyapynov = new(systems);
            LyapynovColorMap colorMap = new LyapynovColorMap();
            MapVisualisation<double> visualisation = new(
                lyapynov,
                "Default name",
                colorMap,
                ShimizyMoriokaDynamicSystem.LAMBDA,
                ShimizyMoriokaDynamicSystem.ALPHA,
                0, 0, 2, 2, Convert.ToDouble, Width: 2, Height: 2);
            FileInfo file;
            // Act
            file = new(creater.Create(visualisation));

            // Assert
            Assert.IsTrue(file.Exists);
        }

        [TestMethod]
        public void Create_LypynovLorenz_test()
        {
            // Arrange
            CreaterImg creater = new();
            FileInfo file;
            LorenzSystems systems = new();
            LyapynovWithProcessCalculation calculation = new(systems);

            calculation[LorenzSystems.SIGMA] = 10;
            calculation[LorenzSystems.R] = 28;
            calculation[LorenzSystems.B] = 8 / 3f;

            DiagrammVisualisation visualisation = new(calculation, "my default name");

            // Act
            file = new(creater.Create(visualisation));

            // Assert
            Assert.IsTrue(file.Exists);
        }
    }
}