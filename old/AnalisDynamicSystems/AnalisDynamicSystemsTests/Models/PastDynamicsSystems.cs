﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystemsTests.Models
{
    internal class PastDynamicsSystems : AnalisDynamicSystems.DynamicSystem
    {
        public PastDynamicsSystems() : base("Past DS", 3) { }
        public override float Fx(Vector3 v, ref Dictionary<string, float> parametrs) => v.X;
        public override float Fy(Vector3 v, ref Dictionary<string, float> parametrs) => v.Y;
        public override float Fz(Vector3 v, ref Dictionary<string, float> parametrs) => v.Z;
        public override Dictionary<string, float> GetDefaultParametrs() => new Dictionary<string, float>();
    }
}
