﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalisDynamicSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using System.Numerics;

namespace AnalisDynamicSystems.Tests
{
    [TestClass]
    public class DynamicSystemTests
    {
        [TestMethod]
        public void GetNextPointTest()
        {
            // Arrange
            var dSystems = new Mock<DynamicSystem>();
            var parametrs = new Dictionary<string, float>();
            dSystems.Setup(x => x.Fx(It.IsAny<Vector3>(), ref parametrs)).Returns(1f);
            dSystems.Setup(x => x.Fy(It.IsAny<Vector3>(), ref parametrs)).Returns(1f);
            dSystems.Setup(x => x.Fz(It.IsAny<Vector3>(), ref parametrs)).Returns(1f);


            // Act
            dSystems.Object.GetNextPoint(new Vector3(0, 0, 0), ref parametrs);


            // Assert
            dSystems.VerifyAll();
        }
    }
}