﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnalisDynamicSystems.ADS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SixLabors.ImageSharp.PixelFormats;
using AnalisDynamicSystems.ConcritDynamicSystems;
using System.Text.Json;
using SixLabors.ImageSharp;
using System.Numerics;

namespace AnalisDynamicSystems.ADS.Tests
{
    [TestClass]
    public class PhasePortraitAttractorTests
    {
        private const string DEFAULT_NAME = "default Name";

        [TestMethod]
        public void GetPhasePortraitAttractorTest()
        {
            // Arrange
            FileInfo file;
            var _params = new Dictionary<string, float>();

            _params[ShimizyMoriokaDynamicSystem.LAMBDA] = 0.8f;
            _params[ShimizyMoriokaDynamicSystem.ALPHA] = 0.6f;

            // Act
            file = new FileInfo(PhasePortraitAttractor.GetPhasePortraitAttractor(
                DEFAULT_NAME, 
                EnumDynamicSystems.ShimizyMoriokaDynamicSystem,
                Params: _params,
                Steap: 0.01f));

            // Assert
            Assert.IsTrue(file.Exists);
        }

        [TestMethod]
        public void GetPhasePortraitAttractor_with_jsonTest()
        {
            // Arrange
            FileInfo file;
            var _params = new Dictionary<string, float>();

            _params[ShimizyMoriokaDynamicSystem.LAMBDA] = 0.8f;
            _params[ShimizyMoriokaDynamicSystem.ALPHA] = 0.6f;

            var json  = JsonSerializer.Serialize(_params);

            Console.WriteLine(JsonSerializer.Serialize(Color.Aqua));

            // Act
            file = new FileInfo(PhasePortraitAttractor.GetPhasePortraitAttractor(
                DEFAULT_NAME,
                EnumDynamicSystems.ShimizyMoriokaDynamicSystem,
                Params: JsonSerializer.Deserialize<Dictionary<string, float>>(json),
                Steap: 0.01f));

            // Assert
            Assert.IsTrue(file.Exists);
        }
    }
}