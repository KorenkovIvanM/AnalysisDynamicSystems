﻿using System.Data.SqlTypes;
using System.Text.Json;
using AnalisDynamicSystems;
using AnalisDynamicSystems.ADS;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using WebAPI.Modules;

namespace WebAPI.Controllers
{
    public class OneLyapynovExponentController : Controller
    {
        private const string
            TYPE_FILE = "application/png";

        [HttpGet("Download")]
        public IActionResult Download(
            string NameFile,
            EnumDynamicSystems dSystem,
            string Params = null,
            float Steap = 0.001f,
            uint CountIteration = 1_000_000)
        {
            var fileName = PhasePortraitAttractor.GetPhasePortraitAttractor(
                NameFile,
                dSystem,
                Steap: Steap,
                Params: JsonSerializer.Deserialize<Dictionary<string, float>>(Params ?? ""),
                CountIteration: CountIteration);

            return PhysicalFile(fileName, TYPE_FILE, fileName.Split("\\").Last());
        }
        [HttpGet("GetBase64")]
        public string GetBase64
            (
            string NameFile,
            EnumDynamicSystems dSystem,
            string Params = null,
            float Steap = 0.001f,
            uint CountIteration = 1_000_000,
            string strBackground = "#00000000",
            string strColorLine = "#ff0000ff",
            int Width = 400,
            int Height = 400
            )
        {
            if (Params == null)
                return PhasePortraitAttractor.GetBase64(
                    NameFile, dSystem,
                    background: new Color().SetHex(strBackground),
                    colorLine: new Color().SetHex(strColorLine),
                    Steap: Steap,
                    Width: Width,
                    Height: Height,
                    CountIteration: CountIteration);
            else
                return PhasePortraitAttractor.GetBase64(
                    NameFile, dSystem,
                    Params: JsonSerializer.Deserialize<Dictionary<string, float>>(Params),
                    background: new Color().SetHex(strBackground),
                    colorLine: new Color().SetHex(strColorLine),
                    Width: Width,
                    Height: Height,
                    Steap: Steap,
                    CountIteration: CountIteration);
        }

        [HttpGet("GetListParametrs")]
        public List<string> GetListParametrs(EnumDynamicSystems dSystem)
            => PhasePortraitAttractor.GetListParametrs(dSystem);
    }
}
