﻿using SixLabors.ImageSharp;
using System.Numerics;

namespace WebAPI.Modules
{
    public static class ConvertToColor
    {
        public static Color SetHex(this Color color, string hex)
        {
            return new Color(new Vector4(
                Convert.ToInt32(hex.Substring(1, 2), 16) / 256f,
                Convert.ToInt32(hex.Substring(3, 2), 16) / 256f,
                Convert.ToInt32(hex.Substring(5, 2), 16) / 256f,
                Convert.ToInt32(hex.Substring(7, 2), 16) / 256f));
        }
    }
}
