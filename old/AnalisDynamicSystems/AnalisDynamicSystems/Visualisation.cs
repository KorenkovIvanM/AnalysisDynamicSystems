﻿using SixLabors.ImageSharp;

namespace AnalisDynamicSystems
{
    public abstract class Visualisation<T>: IVisualisation
    {
        public string Name { get; private set; }
        public string NameDynamicSystems { get; private set; }
        public string TypeOperation { get; private set; }
        public DateTime DateCreate { get; private set; }
        public uint Width { get; private set; }
        public uint Height { get; private set; }
        protected Calculation<T> calculation;
        public abstract Color[,] GetMatrix();

        public Visualisation(Calculation<T> calculation, string Name, uint Width = 400, uint Height = 400)
        {
            this.calculation = calculation;
            this.Name = Name;
            NameDynamicSystems = calculation.GetNameDynamicSystems();
            TypeOperation = calculation.GetType().Name;
            DateCreate = DateTime.Now;
            this.Width = Width;
            this.Height = Height;
        }
    }
}
