﻿using System.Numerics;

namespace AnalisDynamicSystems
{
    public abstract class DynamicSystem
    {
        public string Name { get; private set; }
        public uint Dimension { get; private set; }

        public abstract float Fx(Vector3 v, ref Dictionary<string, float> parametrs);
        public abstract float Fy(Vector3 v, ref Dictionary<string, float> parametrs);
        public abstract float Fz(Vector3 v, ref Dictionary<string, float> parametrs);

        public Vector3 GetNextPoint(Vector3 point, ref Dictionary<string, float> parametrs, double steap = 0.001d)
        {
            float[,] k = new float[4, 3];

            k[0, 0] = Fx(point, ref parametrs);
            k[0, 1] = Fy(point, ref parametrs);
            k[0, 2] = Fz(point, ref parametrs);

            k[1, 0] = Fx(new Vector3((float)(point.X + steap / 2), (float)(point.Y + steap / 2 * k[0, 1]), (float)(point.Z + steap / 2 * k[0, 2])), ref parametrs);
            k[1, 1] = Fy(new Vector3((float)(point.X + steap / 2 * k[0, 0]), (float)(point.Y + steap / 2), (float)(point.Z + steap / 2 * k[0, 2])), ref parametrs);
            k[1, 2] = Fz(new Vector3((float)(point.X + steap / 2 * k[0, 0]), (float)(point.Y + steap / 2 * k[0, 1]), (float)(point.Z + steap / 2)), ref parametrs);

            k[2, 0] = Fx(new Vector3((float)(point.X + steap / 2), (float)(point.Y + steap / 2 * k[1, 1]), (float)(point.Z + steap / 2 * k[1, 2])), ref parametrs);
            k[2, 1] = Fy(new Vector3((float)(point.X + steap / 2 * k[1, 0]), (float)(point.Y + steap / 2), (float)(point.Z + steap / 2 * k[1, 2])), ref parametrs);
            k[2, 2] = Fz(new Vector3((float)(point.X + steap / 2 * k[1, 0]), (float)(point.Y + steap / 2 * k[1, 1]), (float)(point.Z + steap / 2)), ref parametrs);

            k[3, 0] = Fx(new Vector3((float)(point.X + steap), (float)(point.Y + steap * k[2, 1]), (float)(point.Z + steap * k[2, 2])), ref parametrs);
            k[3, 1] = Fy(new Vector3((float)(point.X + steap * k[2, 0]), (float)(point.Y + steap), (float)(point.Z + steap * k[2, 2])), ref parametrs);
            k[3, 2] = Fz(new Vector3((float)(point.X + steap * k[2, 0]), (float)(point.Y + steap * k[2, 1]), (float)(point.Z + steap)), ref parametrs);

            point.X += (float)((steap / 6) * (k[0, 0] + 2 * k[1, 0] + 2 * k[2, 0] + k[3, 0]));
            point.Y += (float)((steap / 6) * (k[0, 1] + 2 * k[1, 1] + 2 * k[2, 1] + k[3, 1]));
            point.Z += (float)((steap / 6) * (k[0, 2] + 2 * k[1, 2] + 2 * k[2, 2] + k[3, 2]));

            return point;
        }
        public virtual Vector3 GetDefaultStartState(ref Dictionary<string, float> parametrs) => new Vector3(0.01f, 0, 0);
        public DynamicSystem(string Name, uint Dimension)
        {
            this.Name = Name;
            this.Dimension = Dimension;
        }
        public abstract Dictionary<string, float> GetDefaultParametrs();
        public virtual List<string> GetListParametrs()
        {
            var list = new List<string>();
            foreach(var item in GetDefaultParametrs())
            {
                list.Add(item.Key);
            }
            return list;
        }
    }
}
