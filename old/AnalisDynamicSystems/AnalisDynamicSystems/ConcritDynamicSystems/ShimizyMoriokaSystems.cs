﻿using AnalisDynamicSystems.ConcritCalculation.Niding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritDynamicSystems
{
    public class ShimizyMoriokaDynamicSystem : DynamicSystem, INiding
    {
        public const string
            LAMBDA = "Lambda",
            ALPHA = "Alphs";
        private static byte
            _dimension = 3;
        private static string
            _name = "Systems Shimizy-Morioka";
        public ShimizyMoriokaDynamicSystem() : base(_name, _dimension) { }
        public override float Fx(Vector3 v, ref Dictionary<string, float> parametrs) 
            => v.Y;
        public override float Fy(Vector3 v, ref Dictionary<string, float> parametrs) 
            => v.X - parametrs[LAMBDA] * v.Y - v.X * v.Z;
        public override float Fz(Vector3 v, ref Dictionary<string, float> parametrs) 
            => -parametrs[ALPHA] * v.Z + v.X * v.X;
        public override Dictionary<string, float> GetDefaultParametrs()
        {
            var parametrs = new Dictionary<string, float>();
            parametrs[ALPHA] = 0.8f;
            parametrs[LAMBDA] = 1f;
            return parametrs;
        }

        public bool IsCritical(Vector3 begit, Vector3 end, ref Dictionary<string, float> _parametrs)
        {
            if (Math.Abs(begit.X) < Math.Sqrt(_parametrs[ALPHA])) return false;
            return (Fx(begit, ref _parametrs) * Fx(end, ref _parametrs)) < 0;
        }
    }
}
