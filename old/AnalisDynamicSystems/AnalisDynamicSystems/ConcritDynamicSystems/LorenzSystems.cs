﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritDynamicSystems
{
    public class LorenzSystems : DynamicSystem
    {
        public const string
            SIGMA = "Sigma",
            R = "R",
            B = "B";
        private static byte
            _dimension = 3;
        private static string
            _name = "Systems Lorenz";
        public LorenzSystems() : base(_name, _dimension) { }
        public override float Fx(Vector3 v, ref Dictionary<string, float> parametrs)
            => parametrs[SIGMA] * (v.Y - v.X);
        public override float Fy(Vector3 v, ref Dictionary<string, float> parametrs)
            => v.X * (parametrs[R] - v.Z) - v.Y;
        public override float Fz(Vector3 v, ref Dictionary<string, float> parametrs)
            => v.X* v.Y - parametrs[B] * v.Z;
        public override Dictionary<string, float> GetDefaultParametrs()
        {
            var parametrs = new Dictionary<string, float>();
            parametrs[SIGMA] = 10;
            parametrs[R] = 28;
            parametrs[B] = 8.0f/3;
            return parametrs;
        }

        public override Vector3 GetDefaultStartState(ref Dictionary<string, float> parametrs)
        {
            return new Vector3(0.01f, 0, 0);
        }
    }
}
