﻿using System.IO;
using System.Numerics;

namespace AnalisDynamicSystems.ConcritCalculation.Lyapynov
{
    public class LyapynovCalculation : Calculation<double>
    {
        public float Eps { get; set; } = 0.001f;

        public LyapynovCalculation(DynamicSystem dynamicSystem) : base(dynamicSystem)
        {
            CountIteration = 1_000_000;
            Steap = 0.01f;
        }

        public override double Act()
        {
            double
                result = 0;
            Vector3
                vector = DSystems.GetDefaultStartState(ref _parametrs),
                closeVector = vector with { Z = Eps };

            for (int i = 0; i < CountIteration; i++)
            {
                vector = DSystems.GetNextPoint(vector, ref _parametrs, Steap);
                closeVector = DSystems.GetNextPoint(closeVector, ref _parametrs, Steap);

                var lengthSeparet = (vector - closeVector).Length();

                result += Math.Log(lengthSeparet / Eps);

                if (result == double.PositiveInfinity || result == double.NegativeInfinity || result == double.NaN) return double.NaN;

                closeVector.X = vector.X + (closeVector.X - vector.X) / lengthSeparet * Eps;
                closeVector.Y = vector.Y + (closeVector.Y - vector.Y) / lengthSeparet * Eps;
                closeVector.Z = vector.Z + (closeVector.Z - vector.Z) / lengthSeparet * Eps;
            }
            return result / (CountIteration * Steap);
        }
    }
}
