﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritCalculation.Lyapynov
{
    public class LyapynovWithProcessCalculation : Calculation<List<double>>
    {
        public float Eps { get; set; } = 0.001f;
        public LyapynovWithProcessCalculation(DynamicSystem dynamicSystem) : base(dynamicSystem)
        {
            CountIteration = 1_000_000;
            Steap = 0.01f;
        }

        public override List<double> Act()
        {
            List<double>
                listResult = new();
            double
                result = 0;
            Vector3
                vector = DSystems.GetDefaultStartState(ref _parametrs),
                closeVector = vector with { Z = Eps };

            for (int i = 0; i < CountIteration; i++)
            {
                vector = DSystems.GetNextPoint(vector, ref _parametrs, Steap);
                closeVector = DSystems.GetNextPoint(closeVector, ref _parametrs, Steap);

                var lengthSeparet = (vector - closeVector).Length();

                result += Math.Log(lengthSeparet / Eps);

                listResult.Add(result / (i * Steap));

                if (result == double.PositiveInfinity || result == double.NegativeInfinity || result == double.NaN) return null;

                closeVector.X = vector.X + (closeVector.X - vector.X) / lengthSeparet * Eps;
                closeVector.Y = vector.Y + (closeVector.Y - vector.Y) / lengthSeparet * Eps;
                closeVector.Z = vector.Z + (closeVector.Z - vector.Z) / lengthSeparet * Eps;
            }

            Console.WriteLine(result/ (CountIteration * Steap));

            return listResult;
        }
    }
}
