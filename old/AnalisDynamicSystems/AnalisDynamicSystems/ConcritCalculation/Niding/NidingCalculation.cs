﻿using System.Numerics;

namespace AnalisDynamicSystems.ConcritCalculation.Niding
{
    public class NidingCalculation : Calculation<int>
    {
        public uint Depth { get; private set; }
        public NidingCalculation(DynamicSystem dynamicSystem, uint Depth, uint countIteration = 10_000, float Steap = 0.01f) 
            : base(dynamicSystem)
        {
            if (!(dynamicSystem is INiding)) throw new ArgumentNullException("Dynamic systems not realisation Niding interface");
            this.Depth = Depth;
            CountIteration = countIteration;
            this.Steap = Steap;
        }

        public override int Act()
        {
            INiding
                niding = (INiding)DSystems;
            Vector3
                oldVector,
                newVector = DSystems.GetDefaultStartState(ref _parametrs);
            int
                result = 0,
                currentDepth = 0;


            for (int i = 0; i < CountIteration; i++)
            {
                oldVector = newVector;
                newVector = DSystems.GetNextPoint(newVector, ref _parametrs, Steap);

                if (niding.IsCritical(newVector, oldVector, ref _parametrs))
                {
                    result = (result << 1) + (Math.Sign(newVector.X) + 1) / 2;
                    if (++currentDepth >= Depth) 
                        break;
                }

                if (float.IsNaN(newVector.X) || float.IsInfinity(newVector.X) || float.IsNegativeInfinity(newVector.X) ||
                    float.IsNaN(newVector.Y) || float.IsInfinity(newVector.Y) || float.IsNegativeInfinity(newVector.Y) ||
                    float.IsNaN(newVector.Z) || float.IsInfinity(newVector.Z) || float.IsNegativeInfinity(newVector.Z))
                    return 0;
            }

            return result;
        }
    }
}
