﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritCalculation.Niding
{
    public interface INiding
    {
        bool IsCritical(Vector3 begit, Vector3 end, ref Dictionary<string, float> _parametrs);
    }
}
