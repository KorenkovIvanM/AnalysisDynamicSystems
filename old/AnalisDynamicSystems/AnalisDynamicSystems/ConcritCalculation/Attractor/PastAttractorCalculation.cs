﻿using SixLabors.ImageSharp;
using System.Numerics;

namespace AnalisDynamicSystems.ConcritCalculation.Attractor
{
    public class PastAttractorCalculation : AttractorCalculation
    {

        public PastAttractorCalculation(DynamicSystem dynamicSystem) : base(dynamicSystem) { }

        public override Vector3[] Act()
        {
            SetSize();

            Vector3 vector = DSystems.GetDefaultStartState(ref _parametrs);
            Vector3[] result = new Vector3[CountIteration];

            for (int index = 0; index < CountIteration; index++)
            {
                vector = DSystems.GetNextPoint(vector, ref _parametrs, Steap);
                result[index] = vector;
            }

            return result;

        }
    }
}
