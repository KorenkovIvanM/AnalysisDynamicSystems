﻿using SixLabors.ImageSharp;
using System.Numerics;

namespace AnalisDynamicSystems.ConcritCalculation.Attractor
{
    public abstract class AttractorCalculation : Calculation<Vector3[]>
    {
        public float MinX { get; private set; }
        public float MinY { get; private set; }
        public float MinZ { get; private set; }
        public float MaxX { get; private set; }
        public float MaxY { get; private set; }
        public float MaxZ { get; private set; }

        protected AttractorCalculation(DynamicSystem dynamicSystem) : base(dynamicSystem) { }

        public void SetSize()
        {
            MinX = float.MaxValue;
            MinY = float.MaxValue;
            MinZ = float.MaxValue;
            MaxX = float.MinValue;
            MaxY = float.MinValue;
            MaxZ = float.MinValue;

            Vector3 vector = DSystems.GetDefaultStartState(ref _parametrs);
            int countIteration = 10_000;
            float _countIteration = 0.05f;

            for (int i = 0; i < countIteration; i++)
            {
                vector = DSystems.GetNextPoint(vector, ref _parametrs, _countIteration);

                if (vector.X < MinX) MinX = vector.X;
                if (vector.Y < MinY) MinY = vector.Y;
                if (vector.Z < MinZ) MinZ = vector.Z;

                if (vector.X > MaxX) MaxX = vector.X;
                if (vector.Y > MaxY) MaxY = vector.Y;
                if (vector.Z > MaxZ) MaxZ = vector.Z;
            }
        }
    }
}
