﻿namespace AnalisDynamicSystems
{
    public abstract class Calculation<T>: ICloneable
    {
        protected Dictionary<string, float> _parametrs;
        public float Steap { get; set; }
        public uint CountIteration { get; set; }
        public float this[string NameParametr]
        {
            get => _parametrs[NameParametr];
            set 
            {
                if (_parametrs.ContainsKey(NameParametr))
                    _parametrs[NameParametr] = value;
                else
                    throw new Exception("Attempt to add a non-existent parameter.");
            }
        }

        protected DynamicSystem DSystems;
        public Calculation(DynamicSystem dynamicSystem)
        {
            DSystems = dynamicSystem;
            _parametrs = dynamicSystem.GetDefaultParametrs();
        }
        public abstract T Act();
        public string GetNameDynamicSystems() => DSystems.Name;

        public object Clone()
        {
            var result = MemberwiseClone();
            ((Calculation<T>)result)._parametrs = ((Calculation<T>)result).DSystems.GetDefaultParametrs();
            return result;
        }
    }
}
