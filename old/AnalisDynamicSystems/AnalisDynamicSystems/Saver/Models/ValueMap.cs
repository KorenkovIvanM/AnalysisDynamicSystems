﻿namespace AnalisDynamicSystems.Saver.Models
{
    public class ValueMap
    {
        public int Id { get; set; }
        public int MapId { get; set; }
        public string Value { get; set; } = null!;
        public int IndexWidth { get; set; }
        public int IndexHeight { get; set; }
        // TODO надо ли мне это?
        //public float ValueWidth { get; set; }
        //public float ValueHeight { get; set; }
    }
}
