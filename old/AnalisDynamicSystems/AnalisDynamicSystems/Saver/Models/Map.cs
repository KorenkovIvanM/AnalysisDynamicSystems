﻿namespace AnalisDynamicSystems.Saver.Models
{
    public class Map
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string NameSystems { get; set; } = null!;
        public string NameCalculation { get; set; } = null!;
        public string NameParametrWidth { get; set; } = null!;
        public string NameParametrHeight { get; set; } = null!;
        public uint Width { get; set; }
        public uint Height { get; set; }
        public float StartWidth { get; set; }
        public float StartHeight { get; set; }
        public float EndWidth { get; set; }
        public float EndHeight { get; set; }
        public float Steap { get; set; }
        public uint CountIteration { get; set; }
        public string? Data { get; set; }
    }
}
