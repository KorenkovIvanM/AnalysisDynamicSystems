﻿using AnalisDynamicSystems.Saver.Models;
using Microsoft.EntityFrameworkCore;

namespace AnalisDynamicSystems.Saver.Context
{
    public class Context : DbContext
    {
        public DbSet<Map> Maps { get; set; }
        public DbSet<ValueMap> Values { get; set; }

        public Context() => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("data source=d:\\test.db");
        }
    }
}
