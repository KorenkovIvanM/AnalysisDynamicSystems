﻿using AnalisDynamicSystems.Saver.Models;

namespace AnalisDynamicSystems.Saver
{
    public delegate T ToolsMapconverter<T>(string item);
    public class ToolsMap<T>
    {
        private Map _map;
        private ToolsMapconverter<T> _converter;

        public ToolsMap(ToolsMapconverter<T> converter)
        {
            _converter = converter;
        }
        public void CreateMap(Map map)
        {
            _map = map;
            using(Context.Context context = new ())
            {
                context.Maps.Add(map);
                context.SaveChanges();
            }
        }
        public void SaveValue(List<T> data, uint IndexWidth)
        {
            using (Context.Context context = new())
            {
                context.Values.AddRange(data.Select((item, index) => new ValueMap()
                {
                    MapId = _map.Id,
                    Value = item.ToString(),
                    IndexWidth = (int)IndexWidth,
                    IndexHeight = index,
                }));
                context.SaveChanges();
            }
        }

        public T[,] GetMatrix(int? MapID = null)
        {
            T[,] result = new T[_map.Width, _map.Height];

            using(Context.Context context = new()) 
            {
                var buff = (
                    from 
                        item in context.Values 
                    where 
                        item.MapId == (MapID == null? _map.Id : MapID ?? 0) 
                    select 
                        item
                    ).ToList();

                for(int i = 0; i < buff.Count; i++)
                    result[buff[i].IndexWidth, buff[i].IndexHeight] = _converter(buff[i].Value);
            }

            return result;
        }

        
    }
}
