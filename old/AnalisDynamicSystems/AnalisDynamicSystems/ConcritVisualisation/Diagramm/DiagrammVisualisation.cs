﻿using SixLabors.ImageSharp;

namespace AnalisDynamicSystems.ConcritVisualisation.Diagramm
{
    public class DiagrammVisualisation : Visualisation<List<double>>
    {
        public Color BackgroundColor { get; set; } = Color.White;
        public Color ColorLinePositive { get; set; } = Color.Red;
        public Color ColorLineNegative { get; set; } = Color.Blue;

        public DiagrammVisualisation(Calculation<List<double>> calculation, string Name, uint Width = 400, uint Height = 400)
            : base(calculation, Name, Width, Height)
        {
        }

        public override Color[,] GetMatrix()
        {
            Color[,] matrix = new Color[Width, Height];

            var data = calculation.Act();
            var max = +5;// data.Max();
            var min = -5;// data.Min();

            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                    matrix[i, j] = BackgroundColor;

            for (int index = 0; index < data.Count; index++)
                if (data[index] != double.NegativeInfinity && data[index] != double.NaN)
                {
                    int indexHeight;
                    if (data[index] > 0)
                        indexHeight = (int)Height - Convert.ToInt32(Math.Round(Height / 2 + data[index] / max * (Height / 2)));
                    else
                        indexHeight = (int)(Convert.ToInt32(Math.Round(data[index] / min * (Height / 2))) + Height / 2);
                    if (indexHeight >= 0 && indexHeight < Height)
                        matrix[(index * Height) / data.Count, indexHeight] = ColorLinePositive;
                }

            for (int i = 0; i < Height; i++) matrix[i, Height / 2] = Color.Black;

            return matrix;
        }
    }
}
