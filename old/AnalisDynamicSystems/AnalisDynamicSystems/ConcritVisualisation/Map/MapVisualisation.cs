﻿using AnalisDynamicSystems.ConcritVisualisation.Map.ColorMap;
using AnalisDynamicSystems.Saver;
using AnalisDynamicSystems.Saver.Models;
using SixLabors.ImageSharp;
using System.Runtime.CompilerServices;

namespace AnalisDynamicSystems.ConcritVisualisation.Map
{
    public class MapVisualisation<T> : Visualisation<T>
    {
        #region delegate
        public delegate
            void SaveRow(ref List<T> row, long indexWidth);
        #endregion
        #region Event
        public event
            SaveRow OnSaveRow;
        #endregion
        public string
            NameParametrWight,
            NameParametrHeight;
        public float
            StartParametrWidth,
            StartParametrHeight,
            EndParametrWidth,
            EndParametrHeight;
        private object
            _lock = new object();
        private IColorMap<T>
            ColorMap;
        private ToolsMap<T>
            _toolsMap;
        public MapVisualisation
        (
            Calculation<T> calculation,
            string Name,
            IColorMap<T> colorMap,
            string NameParametrWight,
            string NameParametrHeight,
            float StartParametrWidth,
            float StartParametrHeight,
            float EndParametrWidth,
            float EndParametrHeight,
            ToolsMapconverter<T> _converter,
            uint Width = 400,
            uint Height = 400
        ) : base(calculation, Name, Width, Height)
        {
            this.NameParametrHeight = NameParametrHeight;
            this.NameParametrWight = NameParametrWight;
            this.StartParametrWidth = StartParametrWidth;
            this.EndParametrWidth = EndParametrWidth;
            this.StartParametrHeight = StartParametrHeight;
            this.EndParametrHeight = EndParametrHeight;
            ColorMap = colorMap;

            Saver.Models.Map _map = new()
            {
                NameSystems = NameDynamicSystems,
                NameCalculation = TypeOperation,
                Name = Name,
                NameParametrWidth = NameParametrWight,
                NameParametrHeight = NameParametrHeight,
                Width = Width, 
                Height = Height,
                StartWidth= StartParametrWidth,
                StartHeight= StartParametrHeight,
                EndWidth= EndParametrWidth,
                EndHeight= EndParametrHeight,
                Steap = calculation.Steap,
                CountIteration = calculation.CountIteration,
                // Data = 
                // TODO допиши данные
            };
            _toolsMap = new ToolsMap<T>(_converter);
            _toolsMap.CreateMap(_map);

            OnSaveRow += MakeSaveRow;
        }

        private void MakeSaveRow(ref List<T> row, long indexWidth)
        {
            _toolsMap.SaveValue(row, (uint)indexWidth);
        }

        public Color[,] GetColorOfMatrix(T[,] result)
        {
            var colorResult = new Color[Width, Height];
            ColorMap.InitializationState(result);
            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                    colorResult[i, j] = ColorMap.GetColor(result[i, j]);
            return colorResult;
        }

        public T[,] GetResultForMatrix()
        {
            Parallel.For(0, Width, indexWight =>
            {
                var result = new List<T>();
                Calculation<T> _calculation = (Calculation<T>)calculation.Clone();
                _calculation[NameParametrWight] = ((float)indexWight / Width) * (EndParametrWidth - StartParametrWidth) + StartParametrWidth;
                for (int indexHeight = 0; indexHeight < Height; indexHeight++)
                {
                    _calculation[NameParametrHeight] = ((float)indexHeight / Height) * (EndParametrHeight - StartParametrHeight) + StartParametrHeight;

                    var buff = _calculation.Act();
                    lock (_lock)
                    {
                        result.Add(buff);
                    }
                }
                lock(_lock) 
                {
                    OnSaveRow?.Invoke(ref result, indexWight);
                }
            });

            return _toolsMap.GetMatrix();
        }

        public override Color[,] GetMatrix() => GetColorOfMatrix(GetResultForMatrix());

        public Color[,] GetImgForMember(int MapId)
            => GetColorOfMatrix(_toolsMap.GetMatrix(MapId));
    }
}
