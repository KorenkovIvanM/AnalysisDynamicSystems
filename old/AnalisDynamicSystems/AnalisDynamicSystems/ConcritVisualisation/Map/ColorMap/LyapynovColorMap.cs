﻿using SixLabors.ImageSharp;

namespace AnalisDynamicSystems.ConcritVisualisation.Map.ColorMap
{
    public class LyapynovColorMap : IColorMap<double>
    {
        private double 
            _max = double.MinValue, 
            _min = double.MaxValue;

        public Color GetColor(double value)
        {
            if (value > 0) return Color.Red;
            if (value < 0) return Color.Blue;
            else return Color.Black;
        }

        public void InitializationState(double[,] data)
        {
            for(int i = 0; i < data.GetLength(0); i++) 
                for(int j = 0; j < data.GetLength(1); j++)
                {
                    if (data[i, j] < _min) _min = data[i, j];
                    if (data[i, j] > _max) _max = data[i, j];
                }    
        }
    }
}
