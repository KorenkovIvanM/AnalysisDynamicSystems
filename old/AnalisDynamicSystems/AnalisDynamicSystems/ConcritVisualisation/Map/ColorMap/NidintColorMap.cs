﻿using SixLabors.ImageSharp;
using System.Numerics;

namespace AnalisDynamicSystems.ConcritVisualisation.Map.ColorMap
{
    public class NidintColorMap : IColorMap<int>
    {
        SortedSet<int> listValue = new SortedSet<int>();
        Dictionary<int, Color> listColor;

        public Color GetColor(int value)
        {
            return listColor[value];
        }

        public void InitializationState(int[,] data)
        {
            for(int i = 0; i < data.GetLength(0); i++)
                for(int j = 0; j < data.GetLength(1); j++)
                    listValue.Add(data[i, j]);

            listColor = new();
            Random random = new Random();
            int f = 0;
            foreach(var item in listValue)
            {
                f++;
                listColor[item] = new Color(
                    new Vector4(
                        (float)(listValue.Count - f)/listValue.Count,
                        (float)random.NextDouble(),
                        (float)f / listValue.Count,
                        (float)random.NextDouble()));
            }
        }
    }
}
