﻿using SixLabors.ImageSharp;

namespace AnalisDynamicSystems.ConcritVisualisation.Map.ColorMap
{
    public interface IColorMap<T>
    {
        void InitializationState(T[,] data);
        Color GetColor(T value);
    }
}
