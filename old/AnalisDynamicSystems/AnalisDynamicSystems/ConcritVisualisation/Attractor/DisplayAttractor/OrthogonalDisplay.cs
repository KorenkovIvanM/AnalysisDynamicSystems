﻿using AnalisDynamicSystems.ConcritCalculation.Attractor;
using AnalisDynamicSystems.ConcritVisualisation.Attractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritVisualisation.Attractor.DisplayAttractor
{
    public class OrthogonalDisplay : Display
    {
        AttractorCalculation
            _calculation;
        Type
            typeVector = typeof(Vector3);
        NameDimension
            NameDimensionWidth,
            NameDimensionHeight;

        public OrthogonalDisplay(
            AttractorCalculation calculation,
            NameDimension NameDimensionWidth,
            NameDimension NameDimensionHeight,
            int Width,
            int Height
        )
        {
            _calculation = calculation;
            this.NameDimensionWidth = NameDimensionWidth;
            this.NameDimensionHeight = NameDimensionHeight;
            Weight = Width;
            this.Height = Height;
        }

        public override int GetPositionPixelHeight(Vector3 vector)
            => getNumberPixel(NameDimensionHeight, vector, Height);

        public override int GetPositionPixelWeight(Vector3 vector)
            => getNumberPixel(NameDimensionWidth, vector, Weight);

        private int getNumberPixel(NameDimension nameDimension, Vector3 vector, int Size)
        {
            //  TODO исправь
            string
                name = Enum.GetName(typeof(NameDimension), nameDimension);
            var
                field = typeVector.GetField(name);
            double
                value = Convert.ToDouble(field.GetValue(vector));

            Type
                attractorCalculation = typeof(AttractorCalculation);
            var
                fieldMax = attractorCalculation.GetProperty("Max" + name);
            double
                Max = Convert.ToDouble(fieldMax.GetValue(_calculation));

            var
                fieldMin = attractorCalculation.GetProperty("Min" + name);
            double
                Min = Convert.ToDouble(fieldMin.GetValue(_calculation));

            return Convert.ToInt32((value - Min) / (Max - Min) * Size);
        }
    }
}
