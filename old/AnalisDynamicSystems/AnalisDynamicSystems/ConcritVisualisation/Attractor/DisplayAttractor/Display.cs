﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritVisualisation.Attractor.DisplayAttractor
{
    public abstract class Display
    {
        public int Weight { get; set; }
        public int Height { get; set; }
        public abstract int GetPositionPixelWeight(Vector3 vector);
        public abstract int GetPositionPixelHeight(Vector3 vector);
    }
}
