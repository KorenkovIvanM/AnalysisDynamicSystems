﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritVisualisation.Attractor
{
    public enum NameDimension
    {
        X,
        Y,
        Z
    }
}
