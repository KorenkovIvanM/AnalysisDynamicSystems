﻿using AnalisDynamicSystems.ConcritCalculation.Attractor;
using AnalisDynamicSystems.ConcritVisualisation.Attractor.DisplayAttractor;
using SixLabors.ImageSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ConcritVisualisation.Attractor
{
    public class AttractorVisualisation : Visualisation<Vector3[]>
    {
        Display
            display;
        Color Background, ColorLine;
        public AttractorVisualisation(
            PastAttractorCalculation calculation,
            string Name,
            uint Width = 400,
            uint Height = 400,
            NameDimension DHeight = NameDimension.X,
            NameDimension DWeight = NameDimension.Y,
            Display? display = null,
            Color? Background = null,
            Color? ColorLine = null)
            : base(calculation, Name, Width, Height)
        {
            this.Background = Background ?? new Color(new Vector4(0, 0, 0, 0));
            this.ColorLine = ColorLine ?? Color.Red;
            this.display = display ?? new OrthogonalDisplay(calculation, DHeight, DWeight, (int)Width, (int)Height);
        }

        public override Color[,] GetMatrix()
        {
            Color[,] result = new Color[Height, Width];
            PastAttractorCalculation? _calculation = calculation as PastAttractorCalculation;

            if (_calculation == null) throw new Exception("The calculation not attractor calculation.");

            for (int i = 0; i < Height; i++)
                for (int j = 0; j < Width; j++)
                    result[i, j] = Background;


            _calculation.SetSize();
            var line = _calculation.Act();


            for (int i = 0; i < line.Length; i++)
            {
                var _x = display.GetPositionPixelWeight(line[i]);
                var _y = Height - display.GetPositionPixelHeight(line[i]);

                if (0 <= _x && _x < Width && 0 <= _y && _y < Height)
                    result[_x, _y] = ColorLine;
            }

            return result;
        }
    }
}
