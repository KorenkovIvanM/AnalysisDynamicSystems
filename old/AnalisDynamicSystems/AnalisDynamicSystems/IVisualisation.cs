﻿using SixLabors.ImageSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems
{
    public interface IVisualisation
    {
        public Color[,] GetMatrix();
    }
}
