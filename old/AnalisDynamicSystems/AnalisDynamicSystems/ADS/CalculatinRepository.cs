﻿using SixLabors.ImageSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalisDynamicSystems.ADS
{
    public abstract class CalculatinRepository
    {
        private const string
            TEMPLATE_NAME_CLASSS = "AnalisDynamicSystems.ConcritDynamicSystems.",
            MESSAGE_ERROR_NOT_EXIST_DYNAMIC_SYSTEM = "Not exist Dynamic systems ";
        public abstract string GetPhasePortraitAttractor(
            string NameFile,
            EnumDynamicSystems dSystem,
            Dictionary<string, float> Params = null,
            float Steap = 0.001f,
            uint CountIteration = 1_000_000);
        public abstract string GetBase64(
            string NameFile,
            EnumDynamicSystems dSystem,
            Dictionary<string, float> Params = null,
            float Steap = 0.001f,
            uint CountIteration = 1_000_000,
            int Width = 400,
            int Height = 400,
            Color? background = null,
            Color? colorLine = null);
        protected DynamicSystem GetSystem(EnumDynamicSystems dSystem)
        {
            Type? type = Type.GetType(TEMPLATE_NAME_CLASSS + dSystem.ToString());
            if (type == null) throw new Exception(MESSAGE_ERROR_NOT_EXIST_DYNAMIC_SYSTEM + dSystem.ToString());
            DynamicSystem? dynamicSystem = Activator.CreateInstance(type) as DynamicSystem;
            if (dynamicSystem == null) throw new Exception(MESSAGE_ERROR_NOT_EXIST_DYNAMIC_SYSTEM + dSystem.ToString());
            return dynamicSystem;
        }

        public List<string> GetListParametrs(EnumDynamicSystems dSystem)
            => GetSystem(dSystem).GetListParametrs();
    }
}
