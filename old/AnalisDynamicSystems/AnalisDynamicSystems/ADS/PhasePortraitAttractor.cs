﻿using AnalisDynamicSystems.ConcritCalculation.Attractor;
using AnalisDynamicSystems.ConcritDynamicSystems;
using AnalisDynamicSystems.ConcritVisualisation.Attractor;
using SixLabors.ImageSharp;

namespace AnalisDynamicSystems.ADS
{
    public class PhasePortraitAttractor : CalculatinRepository
    {
       
        public override string GetPhasePortraitAttractor(
            string NameFile, 
            EnumDynamicSystems dSystem,
            Dictionary<string, float> Params = null,
            float Steap = 0.001f, 
            uint CountIteration = 1_000_000)
        {
            // TODO DRY

            CreaterImg creater = new();
            PastAttractorCalculation calculation = new(GetSystem(dSystem));
            calculation.Steap = Steap;
            calculation.CountIteration = CountIteration;

            Params = Params ?? new();

            foreach(var t in Params )
                calculation[t.Key] = t.Value;
            AttractorVisualisation visualisation = new(calculation, NameFile, DWeight: NameDimension.Z);
            return creater.Create(visualisation);
        }

        public override string GetBase64(
            string NameFile,
            EnumDynamicSystems dSystem,
            Dictionary<string, float> Params = null,
            float Steap = 0.001f,
            uint CountIteration = 1_000_000,
            int Width = 400,
            int Height = 400,
            Color? background = null,
            Color? colorLine = null)
        {

            // TODO DRY
            CreaterImg creater = new();
            PastAttractorCalculation calculation = new(GetSystem(dSystem));
            calculation.Steap = Steap;
            calculation.CountIteration = CountIteration;

            Params = Params ?? new();

            foreach (var t in Params)
                calculation[t.Key] = t.Value;
            AttractorVisualisation visualisation = new(
                calculation, 
                NameFile, 
                DWeight: NameDimension.Z,
                Width: (uint)Width,
                Height: (uint)Height,
                Background: background, 
                ColorLine: colorLine);
            return creater.GetBase64(visualisation);
        }
    }
}
