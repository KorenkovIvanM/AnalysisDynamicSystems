import React from "react"

type App =
{
    icon : React.Component
    title : string
    desc : string
    url : string
}

export { type App }
